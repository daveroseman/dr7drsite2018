<?php

class LocationException extends Exception{}

interface DRLocationIntf
{
    function isValidZipcodeFormat($zipcode);
}

class DRLocation implements DRLocationIntf
{
    const ZIPCODE_PATTERN = '/\d{5}/';
    const OKAY = 'okay';
    const INVALID = 'invalid';
    
    function isValidZipcodeFormat($zipcode='')
    {
        return preg_match(self::ZIPCODE_PATTERN, $zipcode);
    }
    
    static function validateZipcodeList($list=null)
    {
        if( is_null($list) )
        {
            return array(INVALID => 'is_null' );
        } 
        
        if( ! is_array($list) )
        {
            return array(INVALID => 'is_not_array' );
        } 
        
        if( count($list) == 0 )
        {
            return array(INVALID => 'is_empty_list' );
        }

        return array(OKAY => 1);
    }
    
}


