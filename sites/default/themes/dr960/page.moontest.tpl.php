<?php
// $Id: page.tpl.php,v 1.1.2.2.4.2 2011/01/11 01:08:49 dvessel Exp $
?>

<div id="page" class="container-16 clearfix">
    <div id="not-opt">
        <!--[if IE 6]>
            <div><a href="#" class="closex">close</a><span>This site will not work on Internet Explorer 6</span></div>
        <![endif]-->
        <!--[if IE 7]>
            <div><a href="#" class="closex">close</a><span>This site has not been tested to work on Internet Explorer</span></div>
        <![endif]-->
        <!--[if IE 8]>
            <div><a href="#" class="closex">close</a><span>This site has not been tested to work on Internet Explorer</span></div>
        <![endif]-->
        <noscript><div><span>Please enable JavaScript on your browser for the best experience.</span></div></noscript>
    </div><!-- /#not-opt -->
    <div class="clear"></div>
    <div id="site-header" class="clearfix">
        <div id="branding" class="grid-4 clearfix">
        <span id="logo"><a href="/" title="home"><img src="sites/default/img/D-logo-combo-60-pt3.png" alt="DaveRoseman.com"/></a><span>    
        
        <?php if ($site_slogan): ?>Site slogan:
          <div id="site-slogan" class="grid-3 omega"><?php print $site_slogan; ?></div>
        <?php endif; ?>
        </div><!-- /#branding -->

      <?php if ($main_menu_links || $secondary_menu_links): ?>
        <div id="site-menu" class="grid-12">
          <?php print $main_menu_links; ?>
          <?php print $secondary_menu_links; ?>
        </div>
      <?php endif; ?>

      <?php if ($page['search_box']): ?>
        <div id="search-box" class="grid-6 prefix-10"><?php print render($page['search_box']); ?></div>
      <?php endif; ?>
      </div><!-- /#site-header --> 

      <div id="site-subheader" class="prefix-1 suffix-1 clearfix">
      <?php if ($page['highlighted']): ?>
        <div id="highlighted" class="<?php print ns('grid-14', $page['header'], 7); ?>">
          <?php print render($page['highlighted']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page['header']): ?>
        <div id="header-region" class="region <?php print ns('grid-14', $page['highlighted'], 7); ?> clearfix">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
      </div><!-- /#site-subheader -->


      <!-- div id="main" class="column <?php /* print ns('grid-16', $page['sidebar_first'], 4, $page['sidebar_second'], 3) . ' ' . ns('push-4', !$page['sidebar_first'], 4); */ ?>" -->
      <div id="main" class="column <?php print ns('grid-16', $page['sidebar_first'], 5, $page['sidebar_second'], 5) . ' ' . ns('push-3', !$page['sidebar_first'], 4); ?>">
        <?php print $breadcrumb; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>      
        <?php if ($tabs): ?>
          <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php print render($page['help']); ?>

        <div id="main-content" class="region clearfix">
          <?php print render($page['content']); ?>
        </div>

        <?php print $feed_icons; ?>
      </div>

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-left" class="column sidebar region grid-4 <?php print ns('pull-12', $page['sidebar_second'], 3); ?>">
        <?php print render($page['sidebar_first']); ?>
      </div><!-- /#sidebar-left -->
    <?php endif; ?>

    <?php if ($page['sidebar_second']): ?>
      <!-- div id="sidebar-right" class="column sidebar region grid-3" -->
      <div id="sidebar-right" class="column sidebar region grid-5">
        <?php print render($page['sidebar_second']); ?>
      </div><!-- /#sidebar-right -->
    <?php endif; ?>
     
    <div id="footer" class="prefix-1 suffix-1">
    <?php if ($page['footer']): ?>
      <div id="footer-region" class="region grid-14 clearfix">
        <?php print render($page['footer']); ?>
      </div>
    <?php endif; ?>
    </div><!-- /#footer -->

</div>
<script type="text/javascript">
(function($) {  
   // we can now rely on $ within the safety of our "bodyguard" function
    var phase = -1;
   $(document).ready( function() {  
    
        $('#increment-moon').click(function(e){
            e.preventDefault();
            $('#moontest').removeClass('mage_' + phase);
            phase++;
            if( phase > 30){ phase = 0; }
            $('#moontest span').html('phase=' + phase);
            $('#moontest').addClass('mage_' + phase);
        });
        
   }); // ready  
}) ( jQuery );    
</script>
<div id="ft-js" style="background-color:red; padding:4px;">
  <?php //include_once('footer_js.tpl.php'); ?>
</div><!-- ft-js" -->  

