<?php

class GeoUtil
{
    static function isValidZipcodeFormat($zipcode)
    {
        if (preg_match("/^([0-9]{5})$/i",$zipcode))
        {
            return true;
        }
        
        return false;
    }
}