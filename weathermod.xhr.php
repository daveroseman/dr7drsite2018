<?php
/**
 * Hack.
 */
define('DRUPAL_ROOT', getcwd());
define('WEATHERMOD_ROOT','sites/default/modules/weathermod');
require_once(DRUPAL_ROOT . '/includes/bootstrap.inc');
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
include_once( WEATHERMOD_ROOT . '/weathermod.module');
include_once( WEATHERMOD_ROOT . '/weathermod.inc.php');

if(! defined('WEATHERMOD_LOC_KEY') )
{
    define('WEATHERMOD_LOC_KEY','weather_loc');
}

$dataType = ( isset( $_REQUEST['_type'] ) ? $_REQUEST['_type'] : 'html');
$loc =      ( isset( $_REQUEST[WEATHERMOD_LOC_KEY] ) ? $_REQUEST[WEATHERMOD_LOC_KEY] : '');

$debugArr = array(
    'dataType' => $dataType,
    WEATHERMOD_LOC_KEY => $loc,
    'time' => time()
);


// Start headers:
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
switch($dataType)
{
    case 'json':
        header('Content-type: application/json');
        print json_encode($debugArr);
        break;
        
    case 'xml':
        header ("Content-Type: text/xml");
        print "XML: Not handled.";
        break;
        
    case 'html': 
    default:
        header ("Content-Type: text/html");
        print WebWeatherHtml::getWeatherHtml(array(
            WEATHERMOD_USER_LOCATION => $loc,
            WEATHERMOD_AJAX => true
        ));
        
} // end switch block 

