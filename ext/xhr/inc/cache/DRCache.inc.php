<?php
/* Copyright (c) 2012 Dave Roseman for DaveRoseman.com.  All rights reserved. 

   File bootstrap.inc.php MUST load secrets in order for this object to get an SQL connection.
*/

class DRCacheException extends Exception{}

interface DRCacheInterface
{
    public static function getInstance();
	public function get($topic,$key,$supressAutoObject=false);
	public function put($topic, $key, $value,$expires=-1);
    public function clean();
}

abstract class DRCache implements DRCacheInterface
{
    const DISK_CACHE =   1;
    const SQL_CACHE =    2;
    const NOSQL_CACHE =  3;
    const REMOTE_CACHE = 4;
    
    private $cacheType = null;

    private function __construct() 
    {
    }
     
    public function getType()
    {
        return get_class($this);
    }
}

class DRSqlCache extends DRCache implements DRCacheInterface
{
    const CACHE_TABLE = 'TextCache';
    
    private static $selfObj = null;
    var $con;
    
    private function __construct() 
    {
    }
    
    public static function getInstance()
    {
        if( null == self::$selfObj)
        {
            self::$selfObj = new self();
        }
        
        return self::$selfObj;
    } 
    
    protected function getConnection(&$err='')
    {
        return $this->_getMySqlImprovedConnection($err);
    }
        
    protected function _getMySqlImprovedConnection(&$err)
    {
        if($this->con)
        {
            return $this->con;
        }
        
        $this->con = mysqli_connect(
            GENEROUS_CACHE_DB_HOSTNAME, 
            GENEROUS_CACHE_DB_USER, 
            GENEROUS_CACHE_DB_PASSWORD,
            GENEROUS_CACHE_DB_NAME
        );
        
        if($this->con && $this->con->connect_errno)
        {
            $err = array( 
                'error' => "Failed to connect to MySQL: (" . 
                $this->con->connect_errno . ") " . 
                $this->con->connect_error
            );
        }
        
        if(!$this->con)
        {
            $err = array('error' => 'No SQL connection');
        }
        
        return $this->con;
    }

    protected function closeConnection($con=null)
    {
         if(!is_null($con))
        {
            mysqli_close($con);
        }
        
        if(!is_null($this->con) && !false == $this->con)
        {
            mysqli_close($this->con);
            $this->con = null;
        }
    }
    
    // TODO: Bubble up error.
    public function get($topic,$key,$supressAutoObject=false)
    {
        $err = array();
        $mysqli = $this->getConnection($err);
        
        if(!$mysqli) // debug
        {
            return $err;
        }
        
        $sql = 
            'SELECT topic, ckey, value, as_array, ctime, expiry ' . 
            'FROM ' . self::CACHE_TABLE .' '.
            "WHERE topic = '$topic' ".
            "AND ckey = '$key';";
        
        $result = $mysqli->query($sql);
        
        if(!$result)
        {
            return false;
        }
        
        $row = $result->fetch_assoc();
		
        mysqli_free_result($result);
        
        if( 1 == $row['as_array'] && !$supressAutoObject)
        {
            return unserialize($row['value']);
        }
        
        return $row['value'];
    }
    
    // TODO: Bubble up error.
    public function put($topic,$key,$value,$timeToLive=-1)
    {
        $as_array = 0;
        $err = array();
        
        if( empty($topic)) 
        {
            throw new DRCacheException("Bad param: topic=$topic");
        }
        
        if( empty($topic)) 
        {
            throw new DRCacheException("Bad param: key=$key");
        }
        
        if(!is_numeric($timeToLive ))
        {
            throw new DRCacheException("Bad param: TTL=$timeToLive");
        }
        
        
        if(is_array($value))
        {
            $value = serialize($value);
            $as_array = 1;
        }
        
        $value = mysql_real_escape_string($value);
        $sql = null;
      
        if( $timeToLive > 0 )
        {
            $sql = 
                'INSERT INTO '. self::CACHE_TABLE .
                '(topic, ckey, value, as_array, expiry)'.
                ' VALUES '.
                "('$topic', '$key', '$value', $as_array, DATE_ADD( NOW(), INTERVAL $timeToLive SECOND) );";
        }
        else
        {
            $sql =
                'INSERT INTO '. self::CACHE_TABLE .
                '(topic, ckey, value)'.
                ' VALUES '.
                "('$topic','$key','$value');";
        }
        
        return $this->_execSql($sql,$err);
    }
    
    public function clean()
    {
        $sql = 
            'DELETE FROM ' . self::CACHE_TABLE . ' '.
            'WHERE expiry < NOW(); ';
            
        return $this->_execSql($sql);    
    }
    
    private function _execSql($sql,&$err)
    {
        $mysqli = $this->getConnection($err);
        
        if(!$mysqli) 
        {
            return $err;
        }
        
        if ($mysqli->query($sql) === TRUE)
        {
            return true;
        }
        
        return false;
    }
    
    public function __destruct()
    {
        $this->closeConnection();
    }
    
    public function getType()
    {
        return get_class($this);
    }
   
    /* Debuging tool: */
    public function getNow()
    {
        $err = array();
        $mysqli = $this->getConnection($err);
        
        if(!$mysqli) // debug
        {
            return $err;
        }
        
        $sql = 'SELECT NOW() as now;';
        $result = $mysqli->query($sql);
        $row = $result->fetch_assoc();
		$now = $row['now'];
        mysqli_free_result($result);
        return $now;
    }
    
} // end class
