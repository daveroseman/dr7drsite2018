<?php

if(!defined('Y_ZIPCODE_CACHE_TOPIC'))
{
  define('Y_ZIPCODE_CACHE_TOPIC','y_ziplook');
}

if(!defined('ZIPCODE_GEODATA_CACHE_EXPIRES'))
{
    define('ZIPCODE_GEODATA_CACHE_EXPIRES',15552000); // 6 months
}

if(!defined('DAVEROSEMAN_YAHOO_API_APPID'))
{
    // Secret.  Optional.
}

if(!defined('EMPTY_SET'))
{
    define('EMPTY_SET','EMPTY_SET');
}

if(!defined('ERROR'))
{
    define('ERROR','error');
}

if(!defined('Y_RESULT_SET_KEY'))
{
    define('Y_RESULT_SET_KEY','ResultSet');
}

if(!defined('ZIPCODE_FORMAT_NOT_VALID'))
{
    define('ZIPCODE_FORMAT_NOT_VALID','_zipcode_format_not_valid');
}

class DRGeoLookupException extends Exception{}

interface DRGeoLookupIntf
{
    static function getLocationByZipcode($zipcode,&$errPtr);
}

class DRGeoLookup implements DRGeoLookupIntf
{
    static function getLocationByZipcode($zipcode,&$errPtr)
    {
        if(! self::isValidZipcodeFormat($zipcode))
        {
            $errPtr = array(ERROR => ZIPCODE_FORMAT_NOT_VALID);
            return false;
        } 
        
        $locArr = DRSqlCache::getInstance()->get(ZIPCODE_CACHE_TOPIC, $zipcode); 
        
        if( $locArr )
        {
            $locArr['_cached'] = true;
            return $locArr;
        }
        
        $locArr = self::fetchLocationByZipcode($zipcode,$errPtr);
        // print "DEBUG locArr fetched=" .var_export($locArr,true)."\n";
        
        if( false != $locArr  && ! ( true == $locArr['hasError']) ) // Cache the result: 
        {
            $locArr['_cache_time'] = time();
            
            DRSqlCache::getInstance()->put(
                ZIPCODE_CACHE_TOPIC,            // topic
                $zipcode,                       // key
                $locArr,                        // value
                ZIPCODE_GEODATA_CACHE_EXPIRES   // expires (sec)
            );
        }
        
        unset( $locArr['_cached'] );
        $locArr['_fetched'] = true;
        return  $locArr;
    }
    
    static private function fetchLocationByZipcode($zipcode,&$errPtr)
    {
        $lookupUrl = self::getYahooZipcodeLookupUrl($zipcode);
        $responseArr = file($lookupUrl);  // Requires timeout on I/O.
        $phpSer = implode("",$responseArr);
        $zipcodeLookupResult = unserialize($phpSer);
        $validationErrs =  self::getValidationErrorYahooGeoResponse($locationArr);

        if( false != $validationErrs)  
        {
            $errPtr = array(
                ERROR => 'Cannot fetch location by zipcode',
                '_zipcode_lookup_url' => $lookupUrl,
                '_validation_errors' => $validationErrs,
                ZIPCODE_LOOKUP_RESULT => $zipcodeLookupResult,
            );
            
            return false;
        }
        
        return $zipcodeLookupResult;
    }
    
    static function getYahooZipcodeLookupUrl($zipcode)
    {
        return 'http://where.yahooapis.com/geocode?country=USA&postal=' . trim($zipcode).
            '&flags=P&appid='. DAVEROSEMAN_YAHOO_API_APPID;
    }
    
    static function isValidZipcodeFormat($zipcode)
    {
        if (preg_match("/^([0-9]{5})$/i",$zipcode))
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
    
    static function getValidationErrorYahooGeoResponse(&$geoArr)
    {
        if(is_null($geoArr))
        {
            return array(ERROR => 'Y Geodata ptr is null.');
        }
        
        if(!is_array($geoArr))
        {
            return array(ERROR => 'Y Geodata ptr is not an array.');
        }
        
        if( !isset($geoArr[Y_RESULT_SET_KEY]['Error']))
        {
            return array(ERROR => "Y Geodata 'Error' param not found.");
        }
        
        if( 0 != $geoArr[Y_RESULT_SET_KEY]['Error'] )
        {
            return array(ERROR => 'Y_Geodata_flag='. $geoArr[Y_RESULT_SET_KEY]['Error']);
        }
        
        if( !isset($geoArr[Y_RESULT_SET_KEY]['Found']))
        {
            return array(ERROR => "Y Geodata 'Found' param not found.");
        }
        
        if( 0 == $geoArr[Y_RESULT_SET_KEY]['Found'])
        {
            return array(ERROR => EMPTY_SET);
        }
        
        return false;
        
    }//end
    
} // end class

