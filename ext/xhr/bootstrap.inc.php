<?php

include_once('/home/www/html/_p$Rquax6/.private_1.inc.php');

$SECRETS_ARE_LOADED = 1;
if(!isset($SECRETS_ARE_LOADED))
{
    $warn = '<div "color:white; background-color:red; font-size:16px; font-weight:bold; text-align:center; margin:10px; padding:10px;>';
    $warn .= ' PARAMS NOT LOADED. ';
    $warn .= '</div>' . "\n";
    print $warn;
}

if( false == date_default_timezone_set('America/Los_Angeles') )
{
  print "Unable to set timezone.\n";
}

// General keys:
define('ACT_KEY','_act');
define('BYPASS_KEY','_bypass');
define('CACHED','_cached');
define('CRUMB_KEY','_crumb');
define('DEBUG_KEY','_debug');
define('DEFAULT_ZIPCODE','94040');
define('EMPTY_SET','EMPTY_SET');
define('ERROR','error');
define('EXPIRY','_expiry');
define('FETCHED','_fetched');
define('FETCH_TIME','_fetch_time');
define('GEO_LOOKUP_FORMAT','_geo_lookup_format');
define('GEO_LOOKUP_BY_IP_ADDRESS','_geo_lookup_by_ip_address');
define('GEO_LOOKUP_BY_ZIPCODE','_geo_lookup_by_zipcode');
define('GEOLOCATION','_geolocation');
define('LOCATION_LIST','_location_list');
define('LOCATION_REQUEST_LIST','_location_request_list');
define('METADATA','_metadata');
define('NOAA_WEATHER_LOCATION','_NOAA_weather_location');
define('PRECIPITATION','precipitation');
define('REMOTE_ADDR','REMOTE_ADDR');
define('REQUESTED_LOCATION','_requested_location');
define('TEMPERATURE','temperature');
define('TIMESPAN',  '_timespan');
define('UNSERIALIZE_ERROR','_unserialize_error');
define('WEATHER','weather');
define('WEATHER_CACHE_TOPIC','wxlook');
define('WEATHER_CONDITIONS_ICON_URL','_weather_conditions_icon_url');
define('WEATHER_DATA_CACHE_EXPIRES',5400);             // 15 min
define('WEATHER_UNDERGROUND_DATA_CACHE_EXPIRES',5400); // 15 min 
define('WEATHER_LOC_KEY','weather_loc');
define('WEATHER_PARAMS','_weather_params');
define('WX_OFFER_LOCS_FLAG','_wx_offer_locs_flag');
define('WX_USER_ERROR_MSG', '_wx_error_msg');
define('Y_RESULT_SET_KEY','ResultSet');
define('Y_ZIPCODE_CACHE_TOPIC', 'y_ziplook');
define('Y_ZIPCODE_GEO_LOOKUP',  '_y_ziplook');
define('ZIPCODE','_zipcode');
define('ZIPCODE_CACHE_TOPIC','ziplook');
define('ZIPCODE_FORMAT_NOT_VALID','_zipcode_format_not_valid');
define('ZIPCODE_GEODATA_CACHE_EXPIRES',15552000); // 6 months
//define('ZIPCODE_GEODATA_RESPONSE','_zipcode_geodata_response'); 
define('ZIPCODE_LOOKUP_RESULT','_zipcode_lookup_result');
define('ZIPCODE_LOOKUP_ERROR', '_zipcode_lookup_error');

if(!defined('DAVEROSEMAN_YAHOO_API_APPID'))
{
    // Secret.  Optional.
}

//include_once('includes.php');  