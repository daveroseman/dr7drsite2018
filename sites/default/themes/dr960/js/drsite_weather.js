/* drsite_weather.js */
    
DRSITE.weather = {};
DRSITE.weather.xhr_endpoint = 'weathermod.xhr.php';
 
(function($) {  
   // we can now rely on $ within the safety of our "bodyguard" function  
   $(document).ready( function() {  
   
        //----- Weather app --------------
        DRSITE.weather.app = function(){
            return{
                is_started: false,
                isValidZipcode: function(num){
                    if( num )
                    {
                        var zipRegex = /^\d{5}$/;
                        return zipRegex.test(num);
                    }
                    return false;
                },
                getInput: function(){ // Also validate input
                    var o = {};
                    o.weather_loc =  jQuery.trim($('#weather-mod-loc-input').val());
                    o.crumb = $('#weather-mod-form .crumb').val();
                    o.is_zipcode = this.isValidZipcode(o.weather_loc);
                    return o;
                },
                getInputObj: function(loc)
                {
                    var o = {};
                    o.weather_loc =  loc;
                    o.crumb = $('#weather-mod-form .crumb').val();
                    o.is_zipcode = this.isValidZipcode(o.weather_loc);
                    return o;
                },
                update: function(){
                
                    $req_ajax =    $('#weather-mod .waitdata .req_ajax').html() || 0;
                    $saved_loc =   $('#weather-mod .waitdata .saved_loc').html() || '';
                    $default_loc = $('#weather-mod .waitdata .default_loc').html() || '';
                    
                    if( 1 ==  $req_ajax )
                    {
                        if($saved_loc)
                        {
                            var o = this.getInputObj( $saved_loc );
                            this.doSubmit(o);
                            return true;
                        }
                        
                        if($default_loc)
                        {
                            var o = this.getInputObj($default_loc);
                            this.doSubmit(o);
                            return true;
                        }
                        
                        this.setStallMsg(true,"I'm lost.  Please enter something.");
                        return false;
                    }
                },
                setStat: function($txt){ // dev only
                    $('#wxmod-status').html($txt);
                },
                setWait: function(on,$txt){
				
                    var ps = $('#weather-mod .entry .progress .pspin');
                    if( true == on )
                    {
                        ps.show();
                    }
                    else
                    {
                        ps.hide();
                    }
                    $('#weather-mod .entry .progress .pmsg').html($txt);
                },
				setStallMsg: function(on,$txt){
					var ps = $('#weather-mod .entry .progress .pstall');
                    if( true == on )
                    {
                        ps.show();
                    }
                    else
                    {
                        ps.hide();
                    }
                    $('#weather-mod .entry .progress .pstallmsg').html($txt);
				},
				_initTooltip: function(tipid){
				
                    triggerid = tipid + '-trigger';
					
                    $('#' + triggerid).each(function(){
                        $(this).hover( function(){  // hover in
                            $('#' + tipid).toggle();
                        },function(){               // hover out
                            $('#' + tipid).toggle();
                        });
                    });
				},
				initForecast: function(caller){
				
					var tips  = $('#weather-mod #flist').find('div.bn');
					
					if(tips)
                    {
                        $.each(tips,function(i,elt){
                            DRSITE.weather.app._initTooltip( $(this).attr('id') );
                        });
                    }
				},
                setTargetText: function($html){
                    $('#wx-here').html($html);
					DRSITE.weather.app.initForecast('chain');
                },
                saveUserLoc: function($loc){
                    var $tmpLoc = $('#wx-here .sec.loc span.zip').html();
                    if($tmpLoc)
                    {
                        $.cookie("weathermod_loc",$tmpLoc,{ expires : 7 });
                    }        
                },
                doSubmit: function(o){
                    //alert("I will submit. o=" + dump(o));
                    this.setStat("Submitting....");
                    var onBeforeSend = function(){
						DRSITE.weather.app.setStallMsg(false,'');
                        DRSITE.weather.app.setWait(true,'Searching ' + o.weather_loc + ' . . .');
                    };
                    var onSuccess = function(data, textStatus, jqXHR){
                        DRSITE.weather.app.setWait(false,'Loading . . .');
                        DRSITE.weather.app.setTargetText(data);
                        DRSITE.weather.app.saveUserLoc();
                    };
                    var onComplete = function(jqXHR, textStatus){
                        DRSITE.weather.app.setWait(false,'');
                    };
                    var onError = function(jqXHR, textStatus, errorThrown){
                        DRSITE.weather.app.setWait(false,'Error :(');
                    };
                    var wxReqSettings = {
                        url: DRSITE.weather.xhr_endpoint,
                        cache: false,
                        data: o,
                        dataType: "text", // html or text
                        beforeSend: onBeforeSend,
                        success: onSuccess,
                        error: onError,
                        timeout:1500,      // msec
                        statusCode: {
                            403: function(){   
                                DRSITE.weather.app.setStat("HTTP 403, Forbidden.");
                            },
                            404: function(){  
                                DRSITE.weather.app.setStat("HTTP 404, Not found.");
                            },
                            500: function(){   
                                DRSITE.weather.app.setStat("HTTP 500, Internal server error.");
                            }
                        }
                    }; // wxReqSettings
                    jQuery.ajax(wxReqSettings);
                },
                fill: function(){
                   var setBlank = function(){
                        $ip = $('#weather-mod-loc-input');
                        $ip.css('color','#000');
                        $ip.val('');
                        this.is_started = true; 
                   }
                   $('#weather-mod-loc-input')
                        .bind('focus',setBlank)
                        .bind('click',setBlank);
                        
                   if(true != this.is_started)
                   {
                        $ip = $('#weather-mod-loc-input');
                        $ip.css('color','#666');
                        $ip.val('Enter zip code');
                   }
                },
                init: function(){
                    //DRSITE.weather.app.setStat("Loaded ....");
                    DRSITE.weather.app.fill();
                    DRSITE.weather.app.update();
                    var setWarning = function(warn){
                        $lb = $('#weather-mod-form label');
                        $sp = $('#weather-mod-form label span');
                        if(true == warn)
                        {
                            $lb.addClass('warn');
                            $sp.show();
                        }
                        else
                        {
                            $lb.removeClass('warn');
                            $sp.hide();
                        }
                    };
                    $('#weather-mod-form').submit(function(e){
                        oIn = DRSITE.weather.app.getInput();
                        if(!oIn.is_zipcode)
                        {
                            setWarning(true);
                        }
                        else
                        {
                            setWarning(false);
                            DRSITE.weather.app.doSubmit(oIn);
                        }
                    });
                }
            }
        }();
       
        // Initialize objects:
        DRSITE.weather.app.init();
        //---------------------------
   }); // ready  
}) ( jQuery ); 

