<div id="footer-js">
<div id="fb-root"></div><?php /* Required for FB API operations, runs off screen. */ ?>

<?php /* AddThis 
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f4d927442620e07"></script>
*/ ?>
<script type="text/javascript">
/* GA */

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-29521462-1']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* FB */

window.fbAsyncInit = function() {
  FB.init({
    appId      : '329424243775019',
    status     : true, 
    cookie     : true,
    xfbml      : true,
    oauth      : false,
  });
}; 

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>

<?php /* LinkedIn */ ?>
<?php /*
<script type="text/javascript" src="http://platform.linkedin.com/in.js">
  api_key: k7trvlex72g1
  onLoad: onLinkedInLoad
  authorize: false
</script>
*/?>



    </div><!-- /footer-js -->