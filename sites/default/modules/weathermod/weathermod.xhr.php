<?php
//error_reporting(E_ALL);
/**
Set XHR endpoint:
/sites/default/modules/weathermod/weathermod.xhr.php
*/

include_once('weathermod.module');
include_once('weathermod.inc.php');

if(! defined('WEATHERMOD_LOC_KEY') )
{
    define('WEATHERMOD_LOC_KEY','weather_loc');
}

include_once('weathermod.inc.php');
$dataType = ( isset( $_REQUEST['_type'] ) ? $_REQUEST['_type'] : 'html');
$loc =      ( isset( $_REQUEST[WEATHERMOD_LOC_KEY] ) ? $_REQUEST[WEATHERMOD_LOC_KEY] : '');

$debugArr = array(
    'dataType' => $dataType,
    WEATHERMOD_LOC_KEY => $loc,
    'time' => time()
);


// Start headers:
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
switch($dataType)
{
    case 'json':
        header('Content-type: application/json');
        print json_encode($debugArr);
        break;
        
    case 'xml':
        header ("Content-Type: text/xml");
        print "XML: Not handled.";
        break;
        
    case 'html': 
    default:
        header ("Content-Type: text/html");
        print WebWeatherHtml::getWeatherHtml(
            $location,
            array()
        );
       
       //print "HTML response:<br>\n".'<div><pre>' . var_export($debugArr,true) . '</pre></div>';
        
} // end switch block 
/*
// Start output:
print "Yes, dataType=$dataType, loc=$loc<br/>\n";
print "JSON=". json_encode($debugArr); //, JSON_PRETTY_PRINT);
print "<br/>Ends.\n";
*/
exit(0);
