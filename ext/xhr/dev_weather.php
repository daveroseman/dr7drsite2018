<?php

define('BYPASS_SECRET','hwy237');
define('DEBUG_SECRET','bedbug8'); 
define('ACT_KEY','_act');
define('BAD_AJAX_REQUEST_KEY','bad_ajax_request');
define('BYPASS_KEY','_bypass');
define('CRUMB_KEY','_crumb');
define('DEBUG_KEY','_debug');
define('WEATHER_FAULT_KEY','weather_fault');
define('WEATHER_LOC_KEY','weather_loc');

include_once('inc/crumb.inc.php');
include_once('inc/set_response.inc.php');

$_act =    (isset($_REQUEST[ACT_KEY])   ?  $_REQUEST[ACT_KEY] : '');
$_crumb =  (isset($_REQUEST[CRUMB_KEY]) ?  $_REQUEST[CRUMB_KEY] : '');
$_bypass = (isset($_REQUEST[BYPASS_KEY]) && BYPASS_SECRET == $_REQUEST[BYPASS_KEY] ? 1 : 0 );
$_debug =  (isset($_REQUEST[DEBUG_KEY])  && DEBUG_SECRET ==  $_REQUEST[DEBUG_KEY]  ? 1 : 0 );
$isCrumbValid = Crumb::isCrumbValid($_crumb);
 


$_act='weather';
$_REQUEST[WEATHER_LOC_KEY] = '90230';

switch($_act)
{
    case 'weather':
        include_once('inc/location.inc.php');
        include_once('weather/xhr_weather.php');
        $weather_loc = (isset($_REQUEST[WEATHER_LOC_KEY]) ?  $_REQUEST[WEATHER_LOC_KEY] : '');
        print "DEBUG ".WEATHER_LOC_KEY ."=". $weather_loc ."\n";
        $weather = null;
        try
        {
            $weather = SummarizedNOAAWeather::getWeather($weather_loc);
            print json_encode($weather);
        }
        catch(Exception $e)
        {
            $errArr = array( WEATHER_FAULT_KEY => $e->getMessage());
            print json_encode($errArr);
        }
        break;
        
    case 'json_test':
        include_once('tester/json.inc.php');
        $ptr = new JsonPrinter();
        $ptr->addParams(array('foo' => 'bar'));
        $ptr->printReponse();
        break;
        
    default:
        $errArr = array( BAD_AJAX_REQUEST_KEY => 'no_such_act');
        print json_encode($errArr);
        
} // end switch block(_act)

// End.