<?php

define('WXUNDERGROUND_ICON_ROOT_URL','http://icons-ak.wxug.com/i/c/k');

abstract class WundergroundWeatherAbstract extends WebWeather implements WebWeatherIntf
{
    const WUNDERGROUND_WEATHER_REST_ENDPOINT = 'http://api.wunderground.com/api';
    const WUNDERGROUND_API_SECRETY_KEY = '2da27ac83514fe0e';
    static $MAX_DAYS_WEATHER_FORECAST = 6;
    static $WUNDERGROUND_CURRENT_TABLE_KEYS = array(  
        'temp_f' => array(            // real i.e. 58.5
            'ti' => 'Temperature', 
            'dim' => '&deg;F',
        ),            
        'WIND_KEY' => array(      // alias for : wind_dir, wind_mph
            'ti' => 'Wind',
            'dim' => 'mph',
        ),
        'pressure_in' => array(   // real i.e. 30.06
            'ti' => 'Pressure',
            'dim' => 'in. Hg',
        ),
        'relative_humidity' => array(  // int i.e. 51%
            'ti' => 'Rel. humidity',
            'dim' => 1,                // already % in value
        ),
        'dewpoint_f' => array(    // int i.e. 36
            'ti' => 'Dewpoint',
            'dim' => '&deg;F',
        ),
        'visibility_mi' => array(  // real i.e. 10.0
            'ti' => 'Visibility',
            'dim' => 'mi',
        ),
        'UV' => array(             // index i.e. 4.0
            'ti' => 'UV index',
            'dim' => 1
        ),
    );
    
    var $WUNDERGROUND_CURRENT_ICON_KEYS = array(
        'icon'              // i.e. cloudy
    );

    protected static function getWeatherURL($location, $args)
    {
        if( empty($location) && !isset($args[REMOTE_ADDR]) && !empty($args[REMOTE_ADDR]) )
        {
            throw new EmptyLocationException();
            return;
        }
        
        $geoLookupFormat = (isset($args[GEO_LOOKUP_FORMAT]) ? $args[GEO_LOOKUP_FORMAT] : 'zipcode');
        
        $featureArr = array(
            'geolookup',
            'conditions',
            'forecast',
            'astronomy',
            'forecast10day'
        );
        
        $featureString = ( count($featureArr) > 0 ? implode('/',$featureArr) : '' );

        $format = 'json'; // xml
        $query = null;
        
        switch($geoLookupFormat)
        {
            case GEO_LOOKUP_BY_IP_ADDRESS:
            case 'geo_ip':
                $query = 'autoip.' . $format.'?geo_ip=' . $args[REMOTE_ADDR];
                break;
                
            case 'zipcode':
            case GEO_LOOKUP_BY_ZIPCODE:
            default:
                $query = $location . '.' . $format;
                
        }// end switch block
        
        
        return 
            self::WUNDERGROUND_WEATHER_REST_ENDPOINT . '/' . 
            self::WUNDERGROUND_API_SECRETY_KEY .  '/' .
            $featureString .
            '/q/' . $query;
    }
       
    
} // end class

class WundergroundWeather extends WundergroundWeatherAbstract implements WebWeatherIntf
{
    static function getWeather($location, array $args, &$err )
    {
        
        
        $weather = DRSqlCache::getInstance()->get(WEATHER_CACHE_TOPIC, $location);
        
        if( $weather )
        {
            $weather[CACHED] = true;
            return $weather;
        }
        
        //$weather[FETCH_TIME] = time();
        
        // To do:
        // * Key on remote IP address (or at least take IP address into account).  
        //    Be sure to extract zipcode or other key:  IP address alone is not a good key.
        // * Encode city location.
        
        $fetchUrl = self::getWeatherURL($location,$args);
        $weather = self::fetchWeather($fetchUrl,$err);
        
        if( !is_null($weather) && false != $weather )
        {
            DRSqlCache::getInstance()->put(
                WEATHER_CACHE_TOPIC,            // topic
                $location,                       // key
                $weather,                        // value
                WEATHER_UNDERGROUND_DATA_CACHE_EXPIRES   // expires (sec)
            );
        }
        
        unset( $weather[CACHED] );
        return $weather;
    }
    
    protected static function fetchWeather($fetchUrl,&$err)
    {
        $responseArr = file($fetchUrl);
        // To do:  Handle non-xml / non-json responses
        $json = implode("",$responseArr);
        $php = json_decode($json,true); // as assoc arrays
        
        // TO DO: Have predefined error flags.
        if(json_last_error() != JSON_ERROR_NONE)
        {
            $err = array(
                'error' => 'Cannot decode Wunderground json.',
                'json_last_error' => json_last_error(),
                WX_USER_ERROR_MSG => "I'm having trouble talking to the data vendor.  Please try again later."
            );
            return false;
        }
        
        // Check response for error message.
        if( isset($php['response']['error']))
        {
            $err = array(
                'error' => 'Location not found by Wunderground.',
                'raw' => $php['response']['error'],
                WX_USER_ERROR_MSG => "Sorry, I can't find that location.  Please try another.",
                WX_OFFER_LOCS_FLAG => true
            );
            return false;
        }
        
        $php[FETCHED] = true;
        $php[FETCH_TIME] = time();
        //print "DEBUG PHP=" . var_export($php, true) . "\n";
        return self::decodeWeather($php,$err);
        
    } // end fetch
    
    static function decodeWeather($php, &$err)
    {
        // Trim off personal weather stations:
        unset($php['location']['nearby_weather_stations']);
        return $php;
        
    } // end decodeWeather
    
    public static function getWeatherHtml($location, array $args, &$err )
    {
        //$err = array();
        $arr = self::getWeather($location, $args, $err);
        
        if(!$arr)
        {
            return self::getWeatherErrorHtml($arr, $args, $err);
        }
        return self::_getWeatherHtml($arr, $args, $err ); 
    }
    
    private static function _getWeatherHtml($arr, array $args, &$err )
    {

        $obsEpoch =      $arr['current_observation']['observation_epoch'];
        $obsDateRFC822 = $arr['current_observation']['local_time_rfc822'];
        $cobsIcon =      $arr['current_observation']['icon'];
        list($_year,$_month,$_day) = split('-',date("Y-m-j",$obsEpoch));
        $obsTimeStr =  date("g:i a",$obsEpoch);
 
        $_obsDisplayLocArr = $arr['current_observation']['display_location'];
        $req_latitude =  $_obsDisplayLocArr['latitude'];
        $req_longitude = $_obsDisplayLocArr['longitude'];
        $city =          $_obsDisplayLocArr['city'];
        $state =         $_obsDisplayLocArr['state'];
        $country =       $arr['location']['country_name'];
        /*
        if ( 'US' == $country || 'USA' == $country)
        {
            $country = '';
        }
        */
        $sunriseArr = $arr['moon_phase']['sunrise'];
        $sunsetArr  = $arr['moon_phase']['sunset'];

        $sunrise_time = date("g:i a",mktime(
            $sunriseArr['hour'],   // hr
            $sunriseArr['minute'], // min
            0,                     // sec
            $_month,               // month
            $_day,                 // day
            $_year                 // year
        ));        
        $sunset_time =  date("g:i a",mktime(
            $sunsetArr['hour'],   // hr
            $sunsetArr['minute'], // min
            0,                     // sec
            $_month,               // month
            $_day,                 // day
            $_year                 // year
        ));        
        
         $_cobs = $arr['current_observation'];
         
         $_forecast = self::getSimpleForecastDigest($arr);
         
         $debug_caching = '';
         if( isset($arr[CACHED]))
         {
            $debug_caching = 'cached=true;';
         }
         else if(isset($arr[FETCHED]))
         {
            $debug_caching = 'fetched=true;';
         }
         else
         {
            $debug_caching = 'cached=unc  ;';
         }
         
         $debug_fetching =
            'fetch_time='  . date('Y-m-d H:i:s',$arr[FETCH_TIME]) . ';';
        
        // HTML starts here:
        $html = '';
        $html .=  '<div class="wx">' . "\n";
        $html .= '<div class="loc"><span class="city">'.$city.'</span>';
        
        if(!empty($state))
        {
            $html .= '<span class="state">, '.$state.'</div>';
        }
        
        if( !empty($country) )
        {
            $html .= '<span class="country">'.$country.'</span>' ."\n";
        }
        
        $html .= '<div class="geo" style="display:none;"><span class="lat">'.$req_latitude.'</span><span class="lon">'.$req_longitude.'</div>'."\n";
        $html .= '</div>'."\n";
        $html .= '<div class="current">'."\n";
        $html .= '  <span class="as_of_time">As of '.$obsTimeStr.'</span>' . "\n";
        $html .= '<table class="current_obs">' ."\n";
        
        foreach(self::$WUNDERGROUND_CURRENT_TABLE_KEYS as $key => $handler)
        {
            $value = (isset($_cobs[$key]) ? $_cobs[$key] : ''); // handles synethic keys ie WIND_KEY
            $dim =   $handler['dim'];
            $title = $handler['ti'];
            $dimStr = ( 1 != $dim ? $dim : '');
            
            if( 'WIND_KEY' == $key )
            {
                $value = $_cobs['wind_dir']. ' at ' . $_cobs['wind_mph'];
            }
            
            $html .= '<tr><td>' . $title. '</td><td class="'.$key.'">'.$value.' '.$dimStr.'</td></tr>'."\n";
        }
        
        $html .= '</table>' ."\n";
        $html .= '<div><!-- /current -->' ."\n";
        $html .= '<div class="astro">'."\n";
        //$html .= var_export($arr['moon_phase'],true)."\n";
        $html .= '<span class="sunrise">Sunrise: '.$sunrise_time.'</span>';
        $html .= '<span class="sunset">Sunset: '.$sunset_time.'</span>' ."\n";
        $html .= '<div><!-- /current -->' ."\n";
        $html .= '<div class="forecast">'."\n";
        $html .= '<table class="forecast">'. "\n";
        foreach($_forecast as $day)
        {
        
            //$html .= var_export($day,true)."\n";
            //$html .= "----------------------\n";
            $html .= '<tr>';
            $html .= '<td class="wd">'.$day['weekday'].'</td>';
            $html .= '<td class="hi">'.$day['high'].'&deg;</td>';
            $html .= '<td class="lo">'.$day['low'].'&deg;</td>';
            $html .= '<td class="icon"><span class="'. $day['icon'] .'">('.$day['icon'].')</span></td>';
            $html .= '<td class="cdn">'.$day['conditions'].'</td>';
            $html .= '</tr>' . "\n";
            
        }
        $html .= '</table>' ."\n";
        $html .= '<div><!-- /forecast -->' ."\n";
        $html .= '<div class="debug" style="display:none">'."\n";
        $html .=   $debug_caching."\n";
        $html .=   $debug_fetching."\n";
        $html .= '<div><!-- /debug -->' ."\n";
        $html .= '<div class="ft">Data provided by Weather Underground</div>'."\n";
        $html .= '<div><!-- /wx -->' ."\n";
        return $html;
        
    } // end  _getWeatherHtml
    
    private static function getWeatherErrorHtml($w, $args, $err)
    {
        $html = '<div class="wxerror">' ."\n";
        $html .= '<script type="text/javascript">'."\n";
        $html .= '  WX_USER_ERROR = true; '."\n";
        $html .= '</script>'."\n";
        $html .= '<span class="err">Error:</span>'."\n";
        $html .= '<span class="msg">'."\n";
        $html .= $err[WX_USER_ERROR_MSG] ."\n";
        $html .= '</span>' ."\n";
        
        if(isset($err[WX_OFFER_LOCS_FLAG]))
        {
            $cList = self::getCommonWeatherLocationList();//true);
            $first = ' class="first"';
            $html .= '<ul id="wx-common-loc-list">'."\n";
            
            foreach($cList as $key => $value)
            {
                $html .= '<li $first><a href="'.$key.'">'.$value.'</a></li>' . "\n";
                $first = '';
            }
            $html .= '</ul>'."\n";
        }
        
        $html .= '</div><!-- /wxerror -->' ."\n";
        return $html;
    } //end getWeatherErrorHtml
    
    private function getSimpleForecastDigest($raw)
    {
        if(!isset($raw['forecast']['simpleforecast']['forecastday']))
        {
            return false;
        }
        $fc =  $raw['forecast']['simpleforecast']['forecastday'];
        unset($raw);
        
        //print "DEBUG=". var_export($fc,true)."\n"; // debug
        $clean = array();
        
        //$clean[] = 'count='.count($raw); // debug

        
        foreach($fc as $idx => $data)
        {
            if ( $idx >= self::$MAX_DAYS_WEATHER_FORECAST )
            {
                break;
            }
            //$clean[] = "idx $idx = " .var_export($data['date']['epoch'],true);
            
            $clean[] = array(
                'epoch' =>      $data['date']['epoch'],
                'weekday' =>    $data['date']['weekday'],
                'high' =>       $data['high']['fahrenheit'],
                'low'  =>       $data['low']['fahrenheit'],
                'conditions' => $data['conditions'],
                'icon' =>       $data['icon'],
                'hidden' => 1 // placeholder for balloon data
            );
            
        }
  
        return $clean;
    }
    
    // To do:  Encode/decode common locations per Wunderground.com API.
    public static function getCommonWeatherLocationList($USA_only=false)
    {
        $locArr = array(
           'AZ/Phoenix' =>       'Phoenix, AZ',
           'CA/San_Francisco' => 'San Francisco, CA',
           'CA/San_Jose' =>      'San Jose, CA',
           'CA/Los_Angeles' =>   'Los Angeles, CA',
           'DC/Washington'  =>   'Washington, DC',
           'FL/Miami' =>         'Miami, FL',
           'MA/Boston' =>        'Boston, MA',
           'NY/New_York' =>      'New York, NY',
           'OR/Portland' =>      'Portland, OR',
           'WA/Seattle' =>       'Seattle, WA',
        );
        
        if( $USA_only )
        {
            return $locArr;
        }
        
        $locArr['UK/London'] =        'London';
        $locArr['Australia/Sydney'] = 'Sydney';
        $locArr['Japan/Tokyo']  =     'Tokyo';
        $locArr['France/Paris'] =     'Paris';
        
        return $locArr;
    }
} // end class






