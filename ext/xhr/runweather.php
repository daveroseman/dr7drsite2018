<?php

include_once('bootstrap.inc.php');
include_once('inc/crumb.inc.php');
include_once('inc/cache/DRCache.inc.php');
include_once('geo/GeoLookup.inc.php');
include_once('inc/set_response.inc.php');

/*
if(!defined('REMOTE_ADDR'))
{
    print "REMOTE_ADDR is still not defined.\n";
}else{
    print "REMOTE_ADDR is YES defined.\n";
}
print "SERVER=" .var_export($_SERVER,true) ."\n\n";
*/


$_act =    (isset($_REQUEST[ACT_KEY])   ?  $_REQUEST[ACT_KEY] : '');
$_crumb =  (isset($_REQUEST[CRUMB_KEY]) ?  $_REQUEST[CRUMB_KEY] : '');
$_bypass = (isset($_REQUEST[BYPASS_KEY]) && BYPASS_SECRET == $_REQUEST[BYPASS_KEY] ? 1 : 0 );
$_debug =  (isset($_REQUEST[DEBUG_KEY])  && DEBUG_SECRET ==  $_REQUEST[DEBUG_KEY]  ? 1 : 0 );
$userRemoteAddress = ( isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''); 
$isCrumbValid = Crumb::isCrumbValid($_crumb);
 


$_act='weather';
$_REQUEST[WEATHER_LOC_KEY] = 'xxxxx';//'90230';

switch($_act)
{
    case 'weather':
        include_once('inc/location.inc.php');
        include_once('weather/WebWeatherIntf.inc.php');
        //include_once('weather/NOAA_SummaryWeatherClient.inc.php');
        include_once('weather/WundergroundWeatherClient.inc.php');
        $weather_loc = (isset($_REQUEST[WEATHER_LOC_KEY]) ?  $_REQUEST[WEATHER_LOC_KEY] : '');
        $weather = null;
        try 
        {
            $errRef = array();
            $DEBUG_ZIPCODE = DEFAULT_ZIPCODE;
            $DEBUG_LOCATION = DEFAULT_ZIPCODE;
            $DEBUG_REMOTE_IP_ADDRESS = '98.248.44.54';
            $DEBUG_MALFORMED_LOCATION = 'XXXXX';
            $DEBUG_OVERSEAS_LOCATION = 'Japan/Tokyo';
            $weatherArgs = array(
                //GEO_LOOKUP_FORMAT =>GEO_LOOKUP_BY_IP_ADDRESS,
                REMOTE_ADDR => $userRemoteAddress//$DEBUG_REMOTE_IP_ADDRESS
                //$userRemoteAddress
            );
            /*
            $weather = WundergroundWeather::getWeather($DEBUG_LOCATION,$weatherArgs,$errRef);

            if( false == $weather)
            {
                if(isset($errRef['error']))
                {
                    print "Runweather sees error:" .var_export($errRef,true)."\n"; // TEMP
                }
            }
            else
            {
                print "Runweather sees result:\n" . var_export($weather,true)."\n"; // TEMP
            }
            */
            $weatherHtml = WundergroundWeather::getWeatherHtml($DEBUG_LOCATION,$weatherArgs,$errRef);

            print $weatherHtml."\n";
        }
        catch(Exception $e)
        {
            $errArr = array( WEATHER_FAULT_KEY => $e->getMessage());
            print json_encode($errArr);
        }
        break;
        
    case 'json_test':
        include_once('tester/json.inc.php');
        $ptr = new JsonPrinter();
        $ptr->addParams(array('foo' => 'bar'));
        $ptr->printReponse();
        break;
        
    default:
        $errArr = array( BAD_AJAX_REQUEST_KEY => 'no_such_act');
        print json_encode($errArr);
        
} // end switch block(_act)

// End.