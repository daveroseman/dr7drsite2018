
Description:
------------
        
This is a stock quote api module that provides other modules with a facility 
to incorporate stock data in them.

Features:
---------

This module provides several features and options:

* When a stock is first queried, the request goes over the internet to
  Yahoo Finance. The data retrieved is then stored in a database
  table for later fetches by modules.

* The stock data in the table are periodically refreshed from Yahoo,
  at an admin specified interval.

Database:
---------

This module requires that the database table 'stockapi' be created.

Installation:
-------------

- Cron must be configured correctly and running on your site
- Copy the stockapi module directory to the modules directory
- Enable the module in Administer > Modules
- You can change the update frequency in Administer > Settings > StockAPI

API Instructions
----------------
Retrieve Apple's stock quote from Yahoo! 
  
  $stock = stockapi_fetch(array('AAPL'));

Retrieve multiple stocks from Yahoo!

  $symbols = array('AAPL', 'GOOG', 'MSFT', 'SBUX');
  $stocks = stockapi_fetch($symbols);

Caching the stock information locally:

  foreach ($stocks as $key => $stock) {
    stockapi_save(_stockapi_to_object($stock));
  }

Loading a stock from cache

  $stock = stockapi_load('AAPL');

Loading multiple stocks from the cache

  $symbols = array('AAPL', 'GOOG', 'MSFT', 'SBUX');
  $stocks = stockapi_multiload($symbols);

Bugs/Features/Patches:
----------------------

If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/stockapi

Author
------

Originally developed by Matt Westgate http://www.lullabot.com/

Modified and maintained by Khalid Baheyeldin http://baheyeldin.com/khalid
and http://2bits.com

The author(s) can also be contacted for paid customizations of this
and other modules.
