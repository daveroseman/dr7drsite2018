<?php


abstract class GoogleWeather extends WebWeather implements WebWeatherIntf
{
    const GOOGLE_WEATHER_REST_ENDPOINT = 'http://www.google.com/ig/api';
  
    protected static function _fetchWeather($zipCodeList,&$err)
    {
        // Validate zip code list here
        $url = self::getWeatherUrl($zipCodeList,$err);
        $response = file($url);
        return $response;
    }
    
    protected static function getWeatherURL($zipCodeList=array())
    {
        if(count($zipCodeList) == 0)
        {
            throw new EmptyZipCodeListException();
            return;
        }
        
        $zipCodeString = implode('+',$zipCodeList);
        
        return self::GOOGLE_WEATHER_REST_ENDPOINT . 
            '?weather=' . $zipCode;
    }
    
} // end class NOAAWeather

