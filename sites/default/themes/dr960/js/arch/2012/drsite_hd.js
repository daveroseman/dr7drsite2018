/* drsite_hd.js 
   2012-03-02 */

var DRSITE = DRSITE || {};
DRSITE.weather = {};
DRSITE.tweetbox = {};  

(function($) {   
   $(document).ready( function() {
        $('#subfooter-container span.control').click(function(e){
            e.preventDefault();
            $('div.region-subfooter').toggle('fast',function(){
                
                $('a.minus').toggle();
                $('a.plus').toggle();
                
            });
        });   
   }); // ready  
}) ( jQuery ); 

(function($) {  
   // we can now rely on $ within the safety of our "bodyguard" function  
   $(document).ready( function() {  
        //----------------------
        DRSITE.notopt = {
            init: function(){
                $('#not-opt div').click(function(){
                    $(this).hide();
                });
            }
        };
        
        //----- Weather app --------------
        // Weather app temporarily moved to drsite_weather.js for development.
        //---- Twitter box ---------------
        DRSITE.tweetbox.app = function(){
            return{
                init: function(){
                    $("#tweetbox .bd").tweet({
                        join_text: "auto",
                        username: "daveroseman", 
                        count: 10,
                        loading_text: "Loading tweets...",
                        template: "{text}{time}",
                        auto_join_text_default: null,      
                        auto_join_text_ed: null,                   
                        auto_join_text_ing: null,               
                        auto_join_text_reply: null, 
                        auto_join_text_url: null
                    });
                },
                refresh: function($){
                }
            }
        }();
        // Initialize objects:
        DRSITE.notopt.init();
        DRSITE.tweetbox.app.init();
        //---------------------------
   }); // ready  
}) ( jQuery ); 

