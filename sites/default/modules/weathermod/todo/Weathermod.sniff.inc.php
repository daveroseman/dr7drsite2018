<?php

define('WEATHERMOD_LOCATION_SURVEY_TOPIC':'wxloc:');

class WeathermodSniff
{

    protected function sniffDigest(&$digest)
    {
        //print "<PRE>digest=" .var_export($digest,true)."</PRE>\n";
        self::sniffIcon( $digest['current_observation']['icon'] );
       

        if(isset($digest['_SIMPLE_F']))
        {
            foreach($digest['_SIMPLE_F'] as $day)
            {
                self::sniffIcon($day['icon']);
            }
        }
        
        if( isset( $digest['location']['zip'] ) )
        {
            return self::sniffLocation( $digest['location']['zip']  );
        }
        
        // Otherise save (international) city:

        if( isset( $digest['location']['requesturl'] ) )
        {
            return self::sniffLocation( $digest['location']['requesturl']  );
        }
        
        return;
        
    }
    
    private function sniffIcon($icon='')
    {
        if( empty($icon) )
        { 
            return false;
        }
        
        if(! in_array( $icon, self::$WUNDERGROUND_KNOWN_ICONS ) )
        {
            return self::saveHint($icon,'icon');
            //print "Save icon $icon\n";
        }  
    }
    
    private function sniffLocation($loc='')
    {
        if( empty($loc) )
        { 
            return false;
        }
        
        if(! in_array( $loc, self::$WUNDERGROUND_KNOWN_LOCATIONS ) )
        {
            return self::saveHint($loc,'loc');
            //print "Save icon $icon\n";
        }  
    }
     
    private function saveHint($hintStr,$hintType)
    {
        
        $topic = null;
        
        switch($hintType)
        {
            case 'icon':
                $topic = WEATHERMOD_ICON_CACHE_TOPIC;
                break;
                
            case 'loc':
                $topic = WEATHERMOD_LOCATION_SURVEY_TOPIC;
                break;
                
            default:
                return false;
        }
        
        try
        {
            cache_set(      
                $topic . $hintStr, 
                $hintStr,
                'cache', 
                CACHE_PERMANENT
            );
            
            return true;
        }
        catch(Exception $e) 
        {
            return false;
        }
    }
 } // end class