<?php

class WebWeatherException extends Exception{}
class WebWeatherHtmlException extends Exception{}
class EmptyLocationException extends Exception{}
class EmptyZipCodeListException extends WebWeatherException{}

interface WebWeatherIntf
{
    static function getWeather($loc, array $args, &$errPtr);
}

interface WebWeatherHtmlIntf
{
   static function getWeatherHtml($loc, array $args, &$errPtr);
}

abstract class WebWeather implements WebWeatherIntf
{
    const ERROR = 'error';
    const FAULT = 'fault';
}


