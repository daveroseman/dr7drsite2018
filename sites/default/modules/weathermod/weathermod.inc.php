<?php

// weathermod.inc.php
define('WX_LOOKUP_KEY',    '_wx_lookupKey');
define('WX_LOOKUP_FORMAT', '_wx_lookupFormat');

class WebWeatherException extends Exception{}
class WebWeatherHtmlException extends Exception{}
class EmptyLocationException extends Exception{}
class EmptyZipCodeListException extends WebWeatherException{}

interface WebWeatherIntf
{
    static function getWeather($loc, array $args, &$errPtr);
}

interface WebWeatherHtmlIntf
{
   static function getWeatherHtml($loc, array $args, &$errPtr);
}

class AbstractWebWeather // implements WebWeatherIntf
{
    const WUNDERGROUND_WEATHER_REST_ENDPOINT = 'http://api.wunderground.com/api';
    const WUNDERGROUND_API_SECRETY_KEY = 'xxxxxxxxxxxxx';
    const ERROR = 'error';
    const FAULT = 'fault';
    
    //static $MAX_DAYS_WEATHER_FORECAST = 6;
    
    static function getLookupKey($args)
    {
        if( !empty( $args[WEATHERMOD_USER_LOCATION] ) )
        {
            return array(
                WX_LOOKUP_KEY =>      $args[WEATHERMOD_USER_LOCATION],
                WX_LOOKUP_FORMAT  => WEATHERMOD_GEO_LOOKUP_BY_ZIPCODE
            );
        }
        elseif ( !empty( $args[WEATHERMOD_USER_REMOTE_ADDR] ) )
        {
            return array(
                WX_LOOKUP_KEY =>      $args[WEATHERMOD_USER_REMOTE_ADDR],
                WX_LOOKUP_FORMAT  => WEATHERMOD_GEO_LOOKUP_BY_IP_ADDRESS
            );      
        }
        elseif( defined('WEATHERMOD_USER_DEFAULT_LOCATION') )
        {
            return array(
                WX_LOOKUP_KEY =>     WEATHERMOD_USER_DEFAULT_LOCATION,
                WX_LOOKUP_FORMAT  => WEATHERMOD_GEO_LOOKUP_BY_ZIPCODE
            );
        }
        else
        {
            return array(
                'error' => 'WEATHERMOD_USER_DEFAULT_LOCATION not defined.  Please see README.TXT in the installation instructions for this module.'
            );
        }
    }
    
    static function cacheGet($locKey) // assume not empty
    {
        $found = cache_get(WEATHERMOD_DATA_CACHE_TOPIC.$locKey);
      
        if( false == $found) // cache miss
        {
            return false;
        }

        $found = (array)$found;
        // print "DEBUG locKey=$locKey, found=".var_export($found,true)."\n";

        $result = null;
        if( isset($found['data']))
        {
            $result = $found['data'];
            $result['_cached'] = true;
            $result['_fetch_time'] = $found['created'];
            return $result;
        }
        
        return array('error' => 'No data found in Drupal cache');
    }
    
    static function cacheSet($locKey,$weather)
    {
        if(empty($locKey))
        { 
            return false; 
        }
        
        try
        {
            // Throws exception.   Typical error:  Database unavailable.
            cache_set(      
                WEATHERMOD_DATA_CACHE_TOPIC.$locKey, 
                $weather,
                'cache', 
                WEATHERMOD_DATA_CACHE_EXPIRES
            );
            
            return true;
        }
        catch(Exception $e) 
        {
            return false;
        }
    }
    
    protected static function getWeatherURL($args)
    {
        $featureArr = array(
            'geolookup',
            'conditions',
            'forecast',
            'astronomy',
            'forecast10day'
        );
        
        $featureString = ( count($featureArr) > 0 ? implode('/',$featureArr) : '' );
        $format = 'json'; // xml
        $query = null;
        //$geoLookupFormat = $args[WX_LOOKUP_FORMAT];    
        
        switch($args[WX_LOOKUP_FORMAT])
        {
            case WEATHERMOD_GEO_LOOKUP_BY_IP_ADDRESS:
            case 'geo_ip':
                $query = 'autoip.' . $format. '?geo_ip=' . $args[WEATHERMOD_USER_REMOTE_ADDR];
                break;
                
            case 'zipcode':
            case WEATHERMOD_GEO_LOOKUP_BY_ZIPCODE:
                $query = $args[WX_LOOKUP_KEY] . '.' . $format;
                break;
                
            default:
                return array(
                    'error' => 'No lookup format given in getWeatherURL().'
                );
                
        }// end switch block
       
        return 
            self::WUNDERGROUND_WEATHER_REST_ENDPOINT . '/' . 
            self::WUNDERGROUND_API_SECRETY_KEY .  '/' .
            $featureString .
            '/q/' . $query;
        
    } // end func

    
    static function get12HrTime($yr, $mo, $day, $hour24, $minute)
    {
        return date("g:i a",mktime(
            $hour24,   
            $minute,
            0,         // sec
            $mo,               
            $day,                
            $yr                 
        ));
    }
    
    /*
    static function getLocSuggestions()
    {
        $cities = array();
        $cities['US'] = array(
           'CA/San_Francisco' => 'San Francisco',
           'CA/San_Jose' =>      'San Jose',
           'NY/New_York_City' => 'New York',
        );
        $cities['_INTL'] = array(
          'UK/London' => 'London',
          'FR/Paris' => 'Paris',
          'DE/Munich' => 'Munich'
        );
        return $cities;
    }
    */
    
} // emd class

class WebWeather extends AbstractWebWeather // implements WebWeatherIntf
{
   static $WUNDERGROUND_ICON_ROOT_URL = 'http://icons-ak.wxug.com/i/c/k'; // Several styles available from Wunderground.com.
   static $WUNDERGROUND_KNOWN_ICONS = array(  // List icons plus nt_(icon)s
        'chancerain',  
        'chancetstorms',
        'clear',
        'cloudy',         
        'flurries',    // new
        'fog',         // new
        'hazy',        // new
        'mostlycloudy',
        'tstorms',
        'partlycloudy',
        'rain',
        'sleet',      // new
        'snow',
        'nt_chancerain',  
        'nt_chancetstorms',
        'nt_clear',
        'nt_cloudy',         
        'nt_flurries',    // new
        'nt_fog',         // new
        'nt_hazy',        // new
        'nt_mostlycloudy',
        'nt_tstorms',
        'nt_partlycloudy',
        'nt_rain',
        'nt_sleet',      // new
        'nt_snow',
    );
    
    protected static function getWeather($args)
    {
        $lookupKeys = self::getLookupKey($args);
        if(!is_array($args) || count($args) == 0)
        {
            return array('error' => 'No args at getWeather(args).');
        }
        
        $args = array_merge($args,$lookupKeys);
        
        $weather = null;

        if(  WEATHERMOD_CACHE_USE_CACHE )
        {
        	$weather = self::cacheGet($lookupKeys[WX_LOOKUP_KEY]);
        }

        if( $weather )
        {
            //$weather['_cached'] = true;
            return $weather;
        }
        //print "lookupKeys=" . var_export($lookupKeys,true)."\n";
        // print "getWeather() args=" . var_export($args,true)."<br/>";
        
        $weather = self::_fetchWeather( $args );
        $key = $args[WX_LOOKUP_KEY];
        
        if( WEATHERMOD_CACHE_USE_CACHE && !isset( $weather['error'] ))
        {
            self::cacheSet($key,$weather);
        }
        
        $weather['_key'] = $key;
        $weather['_fetched'] = true;
        $weather['_fetch_time'] = time();
        return $weather;
    }
    
    private static function _fetchWeather( $args ) 
    {

        $url = self::getWeatherURL($args);
        //print "DEBUG ... url=". var_export($url,true)." ...\n";
        
        if(is_array($url))
        {
            return $url;
        }

        //print "DEBUG running fetch on url=$url\n";
        
        $fetch = drupal_http_request(
            $url,
            array(              // opts 
                'timeout' => 15 // sec
            )
        );
        
        if(is_object($fetch))
        {
            $fetch = (array)($fetch);
        }
        
       if( 200 != $fetch['code'] )
       {
            //print "Bad fetch, fetch=" .var_export($fetch,true)."\n";
            $status_message = ( isset($fetch['status_message']) ? $fetch['status_message'] : '' );
             
            return array(
                'error' => WEATHERMOD_BAD_FETCH,
                'url'   => $url,
                'code' =>           $fetch['code'],
                'status_message' => $fetch['status_message'] ,
                WEATHERMOD_USER_ERROR_MSG => t('Hmmmm, I think I have a bad connection.')
            );
        }
        
        if( !isset($fetch['data']) )
        {
            return array(
                'error' => WEATHERMOD_BAD_FETCH,
                'detail' => 'Data not found.',
                WEATHERMOD_USER_ERROR_MSG => t('Oops, please try again.')
            );
        }

        $php = json_decode($fetch['data'],true); // as assoc arrays
        //print "<pre>DEBUG php=". var_export($php,true)."</pre>\n";
   
        /*
        if(json_last_error() != JSON_ERROR_NONE)
        {
            return array(
                'error' => 'Cannot decode Wunderground json.',
                'json_last_error' => json_last_error(),
                WEATHERMOD_USER_ERROR_MSG => t('I am having trouble talking to the data vendor.  Please try again later.')
            );
        }
        */
        if( isset($php['response']['error']))
        {
            return array(
                'error' => 'Location not found by Wunderground.',
                'raw' => $php['response']['error'],
                WEATHERMOD_USER_ERROR_MSG => t("Sorry, I can't find that location.  Please try another."),
                WEATHERMOD_OFFER_LOCATIONS_FLAG => true
            );
        }
        
        $digest = self::_digestWundergroundWeather($php);
        self::sniffDigest($digest);
        return $digest;
        
    } // end fetch
    
    protected function sniffDigest(&$digest)
    {
        //print "<PRE>digest=" .var_export($digest,true)."</PRE>\n";
        self::sniffIcon( $digest['current_observation']['icon'] );

        if(isset($digest['_SIMPLE_F']))
        {
            foreach($digest['_SIMPLE_F'] as $day)
            {
                self::sniffIcon($day['icon']);
            }
        }
        return true;
    }
    
    private function sniffIcon($icon='')
    {
        if( empty($icon) )
        { 
            return false;
        }
        
        if(! in_array( $icon, self::$WUNDERGROUND_KNOWN_ICONS ) )
        {
            return self::saveIcon($icon);
            //print "Save icon $icon\n";
        }  
    } 

    private function saveIcon($icon)
    {
        try
        {
            cache_set(      
                WEATHERMOD_ICON_CACHE_TOPIC.$icon, 
                $icon,
                'cache', 
                CACHE_PERMANENT
            );
            
            return true;
        }
        catch(Exception $e) 
        {
            return false;
        }
    }
    
    // Make feed more readable:
    protected static function _digestWundergroundWeather($arr)
    {
        unset($arr['location']['nearby_weather_stations']);
  
        // Get human readable time:
        list($_year,$_month,$_day) = explode('-',date("Y-m-j",$arr['current_observation']['observation_epoch']));
        
        // Digest astro:
        $arr['moon_phase']['current_time']['12hr'] =  self::get12HrTime( $_year, $_month, $_day, $arr['moon_phase']['current_time']['hour'], $arr['moon_phase']['current_time']['minute'] );
        $arr['sunrise'] =  self::get12HrTime( $_year, $_month, $_day, $arr['moon_phase']['sunrise']['hour'], $arr['moon_phase']['sunrise']['minute'] );
        $arr['sunset']  =  self::get12HrTime( $_year, $_month, $_day, $arr['moon_phase']['sunset']['hour'],  $arr['moon_phase']['sunset']['minute']  );
        $arr['_age_of_moon'] = $arr['moon_phase']['ageOfMoon'];
        $arr['_moon_phase_string'] = 'Moon: ' . self::ordinal($arr['_age_of_moon']) . ' day';
        unset($arr['moon_phase']);
   
        // "Simple forecast":
        //$arr['_SIMPLE_F'] = $arr['forecast']['simpleforecast']['forecastday'];
        $arr['_SIMPLE_F'] = self::_digestSimpleForecast($arr);
        unset( $arr['forecast']['simpleforecast']);
        
        $arr['_TEXT_F_DATE'] = $arr['forecast']['txt_forecast']['date'];
                        
        $arr['_TEXT_F'] = self::_digestTextForecast($arr);
        //print "Returning from _digestTextForecast() ...<br>\n";
        
        unset( $arr['forecast']['txt_forecast'] );
     
        return $arr;
    }
        
    private static function _digestSimpleForecast(&$wArr)
    {
       if( !isset($wArr['forecast']['simpleforecast']['forecastday']))
       {
            return array(
                'error' => 'null_simple_forecast',
                WEATHERMOD_ERROR_MSG => 'No simple forecast available.'
           );
       }
       
       $clean = array();
       
       foreach($wArr['forecast']['simpleforecast']['forecastday'] as $idx => $fullDay)
       {
           //print "<PRE>FULLDAY=" .var_export($fullDay,true)."</PRE>\n";
           $dayIdx = strtolower($fullDay['date']['weekday']) . ( $idx >= 14 ? '-1' : '' ); // Allow for forecast > 7 days.
           $clean[$dayIdx] = $fullDay;
       }
       
       return $clean;
    }
    
    private static function _digestTextForecast(&$wArr)
    {
        
       if( !isset($wArr['forecast']['txt_forecast']['forecastday']))
       {
            //print "My error . . .<br>\n";
            return array(
                'error' => 'null_text_forecast',
                WEATHERMOD_ERROR_MSG => 'No forecast available.'
           );
       }
       
        $clean = array();
       
        // Decorate nodes:


        foreach($wArr['forecast']['txt_forecast']['forecastday'] as $idx => $halfDay)
        {
            $splitArr = preg_split("/\s+/",$halfDay['title']);
            $day =   (isset($splitArr[0]) ? $splitArr[0] : '(error 1)');
            $merid = (isset($splitArr[1]) && 'Night' == $splitArr[1] ? 'night' : 'day');
           
            $dayIdx = strtolower($day) . ( $idx >= 14 ? '-1' : '' ); // Allow for forecast > 7 days.
            
            if( !isset( $clean[$dayIdx] ) )
            {
                $clean[$dayIdx]['_day_idx'] = $dayIdx;
            }
            
            // Patch icons:  Nighttime text forecast should show nt_icon and not sun.
            //$halfDay['_icon'] = ('night' == $merid ? 'nt_' : '' ) . $halfDay['icon']; // patch
            $halfDay['_icon'] = $halfDay['icon'];  // Omit patch
	    $halfDay['_icon_url'] = self::$WUNDERGROUND_ICON_ROOT_URL .'/'. $halfDay['_icon'] . '.gif';
            $clean[$dayIdx][$merid] = $halfDay;
            
           
        } // foreach
        
        //print "</pre>";
        return $clean;
        
    } // _digestTextForecast()
    
    // Copied from php.net:
    private function ordinal($num)
    {
        // Special case "teenth"
        if ( ($num / 10) % 10 != 1 )
        {
            // Handle 1st, 2nd, 3rd
            switch( $num % 10 )
            {
                case 1: return $num . 'st';
                case 2: return $num . 'nd';
                case 3: return $num . 'rd';                
            }
        }
        // Everything else is "nth"
        return $num . 'th';
    }
    
    public static function test($testLoc)
    {
        $args[ WEATHERMOD_USER_LOCATION ] = $testLoc;
        $args[ WEATHERMOD_USER_REMOTE_ADDR] = ip_address();
        //print "WEATHERMOD_USER_REMOTE_ADDR=" .$args[ WEATHERMOD_USER_REMOTE_ADDR]. "\n";
        //print '<pre>Test args=' . var_export($args,true). "</pre>\n";
        $weather = self::getWeather($args);
        //print '<pre>' . var_export($weather,true). "</pre>\n";
    }
}

class WebWeatherHtml extends WebWeather
{    

    static $WUNDERGROUND_CURRENT_TABLE_KEYS = array(  
        'temp_f' => array(            // real i.e. 58.5
            'Temperature', 
            '&deg;F',
        ),            
        'WIND_KEY' => array(      // alias for : wind_dir, wind_mph
            'Wind',
            'mph',
        ),
        'pressure_in' => array(   // real i.e. 30.06
            'Pressure',
            'in. Hg',
        ),
        'relative_humidity' => array(  // int i.e. 51%
            'Rel. humidity',
            '',                // already % in value
        ),
        'dewpoint_f' => array(    // int i.e. 36
            'Dewpoint',
            '&deg;F',
        ),
        'visibility_mi' => array(  // real i.e. 10.0
            'Visibility',
            'mi',
        ),
        'UV' => array(             // index i.e. 4.0
            'UV index',
            ''
        ),
    );
    
    /*
    static function getLocSuggestionsHtml() 
    {
        $cities = self::getLocSuggestions();
        
        $html = '<div class="sec sug_cit">';
        $html .= '<ul class="grp US">' ."\n";
        
        foreach($cities['US'] as $key => $value)
        {
            $html .= '<li><a href="' . $key . '">' . $value . '</a></li>';
        }
        
        $html .= '</ul>' ."\n";
        $html .= '<ul class="grp _INTL">' ."\n";
        
        foreach($cities['_INTL'] as $key => $value)
        {
            $html .= '<li><a href="' . $key . '">' . $value . '</a></li>';
        }
        
        $html .= '</ul>' ."\n";      
        $html .= '</div><!-- /sug_cit -->' ."\n";
        return $html;
    }
    */
    
    
    static function getWeatherHtml($args)
    {
    
        if( WEATHERMOD_LOAD_QUICK_ONLY == $args[WEATHERMOD_LOAD_SPEED] )
        {
            return  self::_getWeatherWaitHtml($args);
        }
        
        $weather = self::getWeather($args);
        // DEBUG:
        //print '<pre>'.var_export($weather,true)."</pre>\n";
        
        if( false == $weather ) // show spinner, wait for AJAX response.
        {
            return  self::_getWeatherWaitHtml($args,'(Really bad getWeather() call.)');
        }
        
        if(  isset( $weather[WEATHERMOD_USER_ERROR_MSG] ) || isset( $weather['error'] )  )
        {
            $ehtml = '<div class="wx_error">' ."\n";
            $ehtml .= '<script type="text/javascript">'."\n";
            $ehtml .= '  _weathermod_user_error = true; '."\n";
            $ehtml .= '</script>'."\n";
            
            if( isset( $weather[WEATHERMOD_USER_ERROR_MSG] ) )
            {
                $ehtml .= '<p class="msg">' . $weather[WEATHERMOD_USER_ERROR_MSG] . '</p>' ."\n";
            }
            else
            {
                $ehtml .= '<p class="error">Undefined error: ' . $weather['error'] . '</p>' ."\n";
            }
            
            $ehtml .= '</div><!-- /wx_error -->' ."\n";
            return $ehtml;
        }
        
        $html = '';
        $html .= self::_getDisplayLocation($weather);
        $html .= self::_getCurrentCond($weather);
        $html .= self::_getAstro($weather);
        $html .= self::_getForecast($weather);
        //$html .= self::_getTextForecast($weather);
        $html .= '<script type="text/javascript"> _weathermod_result_html_okay = true; </script>' . "\n";
        return $html;
    }
    
    static private function _getWeatherWaitHtml($args)
    {
        $debugMsg =   (  isset($args['debug_msg']) ? $args['debug_msg'] : '' );
        $savedLoc =   ( !empty($args[WEATHERMOD_USER_SAVED_LOCATION]) ? $args[WEATHERMOD_USER_SAVED_LOCATION] : '' );
        $defaultLoc = ( !empty($args[WEATHERMOD_USER_DEFAULT_LOCATION_KEY]) ? $args[WEATHERMOD_USER_DEFAULT_LOCATION_KEY] : '' );
        
        $wait = '<div class="mainw wait">' ."\n";
        $wait .= '<span class="waitmsg">' . t('Please wait ...') . $debugMsg . '</span><br/>'. "\n";
        $wait .= '<div class="waitimg"></div>'. "\n";
        $wait .= '<div class="waitdata" style="display:none;">'. "\n";
        $wait .= '  <span class="req_ajax">1</span>'. "\n";
        $wait .= '  <span class="saved_loc">' . $savedLoc . '</span>'. "\n";
        $wait .= '  <span class="default_loc">' . $defaultLoc . '</span>'. "\n";
        $wait .= '</div>'. "\n";        
        $wait .= '</div><!-- /wait -->'."\n";
        return $wait;
    }
    
    static private function _getDisplayLocation(&$weather)
    {
        $_reqLoc = $weather['location'];
        $city =    $_reqLoc['city'];
        $state =   (isset($_reqLoc['state']) ? $_reqLoc['state'] : '');
        $zip =     (isset($_reqLoc['zip'])   ? $_reqLoc['zip']   : '');
        $country = $_reqLoc['country_name'];
        if( 'USA' == $country ){ $country = ''; }
        $reqLat = $_reqLoc['lat'];
        $reqLon = $_reqLoc['lon'];
        
        $html = '<div class="sec loc">';
        $html .=   '<span class="city">'.$city.',</span> ';
        
        if(!empty($state))
        {
            $html .= '<span class="state">' . $state . '</span> ';
        }
        
        if(!empty($zip))
        {
            $html .= '<span class="zip">' . $zip . '</span> ' . "\n";
        }
        
        $html .=   '<span class="country">'.$country.'</span> ' ."\n";
        $html .=   '<div class="geo" style="display:none;">'."\n";
        $html .=   '  <span class="lat">'.$reqLat.'</span><span class="lon">'.$reqLon.'</span>' . "\n";
        $html .=   '</div><!-- /geo -->'."\n";
        $html .= '</div><!-- /loc -->' . "\n"; // loc
        return $html;

    }//_getDisplayLocation()
    
    static private function _getCurrentCond(&$weather)
    {
        $_cobs = $weather['current_observation'];
        $obsTimeStr = date(
            "g:i a",
            $_cobs['observation_epoch']
        );
        $currWeatherStr = $_cobs['weather'];
        $iconUrl = $_cobs['icon_url'];
        $icon  =   $_cobs['icon'];
        
        $html = '<div class="sec current">'."\n";
        $html .= '  <div class="as_of_time">As of '.$obsTimeStr.'</div>' . "\n";
        $html .= '  <div class="chd clearfix">'. "\n";
        $html .= '    <span class="curr_icon"><img src="'.$iconUrl.'" alt="'.$icon.'" class="wu_icon"/></span>' . "\n";
        $html .= '    <span class="curr_wstr">' . $currWeatherStr . '</span>' . "\n";
        $html .= '  </div><!-- /chd -->'. "\n";
        $html .= '  <table class="current_obs">' ."\n";
        
        foreach(self::$WUNDERGROUND_CURRENT_TABLE_KEYS as $key => $dim)
        {
            list($title, $dimStr) = $dim;
            $value = (isset($_cobs[$key]) ? $_cobs[$key] : '');
            if( 'WIND_KEY' == $key )
            {
                $value = $_cobs['wind_dir']. ' at ' . $_cobs['wind_mph'];
            }
            $html .= '<tr><td class="ti">' . $title. '</td><td class="val '.$key.'">'.$value.' '.$dimStr.'</td></tr>'."\n";
        }
        
        $html .= '  </table>' ."\n";
        $html .= '</div><!-- /current -->' ."\n";
        return $html;
        
    }//_getCurrentCond()
    
    static private function _getAstro(&$weather)
    {
        $sunrise =      $weather['sunrise'];
        $sunset =       $weather['sunset'];
        $ageOfMoon =    $weather['_age_of_moon'];
        $moonPhaseStr = $weather['_moon_phase_string'];
        
        $html = '<div class="sec astro">' . "\n";
        $html .= '<div class="sun">';
        $html .= '<span class="sunrise">Sunrise: '. $sunrise . '</span>';
        $html .= '<span class="sunset">Sunset: '  . $sunset .  '</span>' ."\n";
        $html .= '</div>';
        $html .= '<div class="moon mage_'.$ageOfMoon.'"><span>'.$moonPhaseStr.'</span></div>'; 
        $html .= '</div><!-- /astro -->' ."\n";
        return $html;
    }
      
    static private function _getForecast(&$weather)
    {
        $html = '<div class="sec forecast">'. "\n";
        $html .= '<h3>'. t('Forecast') .'</h3>'. "\n";
        
        
        if( isset($weather['_SIMPLE_F']['error']))
        {
            $html .= '<p class="none">' . $weather['_SIMPLE_F'][WEATHERMOD_ERROR_MSG] . '</p>' . "\n";
            $html .= '</div><!-- /forecast (return early) -->' ."\n";
            return $html;
        }
        
        $html .= '<div id="flist" class="fore">'. "\n"; // was: table
        $ct = -1;
        foreach($weather['_SIMPLE_F'] as $didx => $day)
        {
            $ct++;
            if( 0 == $ct ) // Skip today.
            {
                continue;
            }
			
            $first = ( $ct <= 1 ? ' first' : '' );
            $wkday = strtolower( $day['date']['weekday'] );
            $html .= '<div class="day'.$first.'" id="wx-' . $wkday . '-trigger">'. "\n";
            $html .= '  <div class="simplef">';
            $html .=     '<span class="wd">' . $day['date']['weekday'] . '</span>';
            $html .=     '<div class="conds">';
            $html  .=      '<img height="24" width="24" '.
                           ' src="'.$day['icon_url'].'" ' .
                           ' alt="'.$day['icon'].'" '.
                           ' title="'.$day['conditions'].'" class="wu_icon"/>';
            $html .=     '</div>';
            $html .=     '<div class="temps">';
            $html .=       '<span class="hi">' . $day['high']['fahrenheit']    .  '&deg;</span> <span class="sep">/</span> ';
            $html .=       '<span class="lo">' . $day['low']['fahrenheit']     .  '&deg;</span>';
            $html .=     '</div>' . "\n";
            $html .= "  </div><!--/ simplef -->\n";
            $html .=    self::_getTextForecastDay($weather,$didx);
            $html .= "</div><!-- /day ($wkday) -->\n";
           
            if($ct >= WEATHERMOD_MAX_DAYS_WEATHER_FORECAST )
            {
                break;
            }
            
        }// foreach
        
        $html .= '</div><!-- /forecastlist -->' ."\n";
        $html .= '</div><!-- /forecast -->' ."\n";
        return $html;
        
    } // _getForecast(&$weather)
    
    
    static private function _getTextForecastDay(&$weather,$didx)
    {
        if( isset($weather['_TEXT_F']['error']))
        {
            $html .= '<p class="none">' . $weather['_TEXT_F'][WEATHERMOD_ERROR_MSG] . '</p>' . "\n";
            return $html;
        }
		
		$wday = $weather['_TEXT_F'][$didx];
			//$debugStyle = ( 'wednesday' == $didx ? '' : 'style="display:none;"');
		    //$html = '<div class="tooltip" id="wx-' . $day['_day_idx'] . '" style="display:none;">' . "\n"; // Set child forecast id.
			// $html =  '<div class="bn" id="wx-' . $didx . '" >' . "\n"; // Set child forecast id.
			
			$html =  '<div class="bn" id="wx-' . $didx . '"style="display:none;" >' . "\n";
			//$html =  '<div class="bn" style="visibility:hidden;" >' . "\n"; 			
            $html .=   '<div class="top">';
			$html .=     '<div class="inner"><span class="wkday">' . $wday['day']['title'] .'</span></div>' . "\n";
			$html .=   '</div>' ."\n"; // top
			$html .=   '<div class="mid">' . "\n";
			$html .=     '<div class="half dayt">' . "\n";
			$html .=  self::_getBalloonHalfday($wday,'day');
			$html .=     '</div>' . "\n"; 
            $html .=     '<div class="half nightt">' . "\n";
            $html .=  self::_getBalloonHalfday($wday,'night');
            $html .=     '</div>' . "\n";
			$html .= '  </div>' ."\n"; // mid
			$html .= '<div class="bot"></div>' . "\n";
			//$html .= '<pre>DEBUG:' . var_export($wday,true) . '</pre>';
            $html .= '</div>' . "\n";
			return $html;
    }
	
	private static function _getBalloonHalfday(&$wday,$dn)
	{
		$html = '<div class="dntop">';
		$html .= '<img src="' .  $wday[$dn]['_icon_url'] . '" title="' . $wday[$dn]['_icon'] . '" alt="' . $wday[$dn]['_icon']. '" width="24" height="24"/>';
		$html .= '<span class="dn">' . ('night' == $dn ? 'night' : 'day' ) . '</span><br/>';
		$html .= '</div>' ."\n";
		$html .= '<p class="fcttext">' . $wday[$dn]['fcttext'] . '</p>' . "\n";
		return $html;
	}
 
    
    static function getPrecachedImageHtml()
    {
        $html = '<div class="pcim">'."\n";
        foreach(self::$WUNDERGROUND_KNOWN_ICONS as $icon)
        {
            $url = self::$WUNDERGROUND_ICON_ROOT_URL . '/' . $icon . '.gif';
            $html .= '<img src="' . $url . '" alt="' . $icon . '" title="' . $icon . '"/>';
        }
        
        $WX_LOCAL_IMAGES = array(
            'wx_bn_top_rtc.png' => 'balloon top',
            'wx_bn_bot_rtc.png' => 'balloon bottom'
         );
		// D. Roseman custom balloon w/ callout:
        
        foreach($WX_LOCAL_IMAGES as $icon => $alt )
        {
            $html .= '<img src="'. WEATHERMOD_CUSTOM_BALLOON_PATH . '/'. $icon .'" alt="' .$alt. '"  />';
        }
        $html .= '</div>'."\n";
        return $html;
    }
    
    static function test($testLoc)
    {
        $args = array();
        $args[ WEATHERMOD_USER_LOCATION ] = $testLoc;
        $args[ WEATHERMOD_USER_REMOTE_ADDR] = ip_address();
        //print "WEATHERMOD_USER_REMOTE_ADDR=" .$args[ WEATHERMOD_USER_REMOTE_ADDR]. "\n";
        //print '<pre>Test args=' . var_export($args,true). "</pre>\n";

        // getWeatherHtml($loc, array $args, &$errPtr)
        $loc = "do_not_use"; // debug, untesed
        $errPtr = array();   // debug, untested
        $weather = self::getWeatherHtml($loc, $args, $errPtr);
        print '<pre>' . var_export($weather,true). "</pre>\n";
    }
    
} // end class WebWeather



