/* drsite_hd.js 
   2012-02-26 */
   
var DRSITE = DRSITE || {};

DRSITE.notopt = {
    init: function($){
        $('#not-opt div').click(function(){
            $(this).hide();
        });
    }
};
DRSITE.weather = {}
DRSITE.weather.app = function($){
    return{
        is_started: false,
        doSubmit: function($){
            var _crumb = $('#weather-mod-form crumb').val();
            var loc = $('#weather-mod-loc-input').val();
            alert("I shall submit loc=" + loc + ", _crumb=" + _crumb);
        },
        getInput: function($){
            //var _crumb = $('#weather-mod-form crumb').val();
            //var loc = $('#weather-mod-loc-input').val();
            o = {};
            o._crumb = $('#weather-mod-form crumb').val();
            o.loc =  $('#weather-mod-loc-input').val();
            return o;
        },
        validate: function($){
        
        },
        run: function($){
            if( true == this.is_started )
            {
                var oIn = this.getInput($);
                alert("Input=" + dump(oIn));
                return;
            }
            alert("Still not ready.");
        },
        fill: function($){
            if(true != this.is_started){
                $ip.css('color','#000');
                $ip.val('');
                this.is_started = true;
            }
        },
        init: function($){
            $ip = $('#weather-mod-loc-input');
            $ip.css('color','#666');
            $ip.val('Enter zip code');
            $ip.bind(
                'focus',this.fill($)
            ).bind(
                'click',this.fill($));
                
            $('weather-input-button').click(this.run($));
        }
    }
}();

DRSITE.tweetbox = {};
DRSITE.footer = {};
DRSITE.tweetbox.app = function($){
    return{
        init: function($){
            $("#tweetbox .bd").tweet({
                join_text: "auto",
                username: "daveroseman", //"namcogames",//"seaofclouds",
                //avatar_size: 32,
                count: 5,
                loading_text: "Loading tweets...",
                template: "{text}{time}",
                auto_join_text_default: null,      
                auto_join_text_ed: null,                   
                auto_join_text_ing: null,               
                auto_join_text_reply: null, 
                auto_join_text_url: null
            });
        },
        refresh: function($){
        }
    }
}();
DRSITE.footer.init = function($){
    //$('#tool-drupal').hover();
    //$('#tool-jquery').hover();
    //$('#tool-jquery').hover();
}


jQuery(document).ready(function($) {
  DRSITE.notopt.init($);
  DRSITE.weather.app.init($);  
  DRSITE.tweetbox.app.init($);
  $('#tweetbox-refresh').click(function(){
      DRSITE.tweetbox.app.refresh($);
  });
  DRSITE.footer.init($);
});   