<?php

define('CRUMB_SALT','Xf6@72!8');

// TODO:  Test for stale crumb.

class Crumb
{
    const DELIM = '_';
    const CRUMB_PATTERN = "/_/";
    const VERSION = 1;
    
    static function getCrumb($makeFail=false)
    {
        $longtime = self::getLongtime();
        $hash = self::encode($longtime,$makeFail);
        return $longtime . self::DELIM . $hash;
    }
    
    static function isCrumbValid($crumb='')
    {
        if( empty($crumb) )
        { 
            return false; 
        }
        
        $arr = preg_split(self::CRUMB_PATTERN, $crumb);  // Function 'split()' is now deprecated.
        //print "Debug arr=" .var_export($arr,true)."\n";
        
        if( count($arr) != 2 )
        {
            return false;
        }
        
        $raw =    $arr[0];
        $digest = $arr[1];
        //echo "raw=$raw \ndigest=$digest \nencoded=" .self::encode(trim($raw)); // debug
        
        if( empty($raw) || empty($digest))
        {
            return false;
        }
        
       
        
        if( self::encode(trim($raw)) == trim($digest))
        {
            return true;
        } 
        
        return false;
    }
    
    private static function getLongtime()
    {
        list($usec, $sec) = explode(" ", microtime());
        return $sec . self::pad($usec);
    }
    private static function pad($usec_decimal)
    {  
        return substr($usec_decimal,2,3); 
    }
    
    private static function encode($hvar,$makeFail=false)
    {
        if( $makeFail ) // For testing only.
        {
            return substr( md5( $hvar ), 0, 12);
        }
        
        return substr( md5( $hvar . CRUMB_SALT ), 0, 12);
    }
    
    static function getCrumbTest()
    {
        $goodCrumb = self::getCrumb(false);
        $badCrumb =  self::getCrumb(true);
        $goodCrumbResult = self::isCrumbValid($goodCrumb);
        $badCrumbResult  = self::isCrumbValid($badCrumb);
        $nullCrumbResult = self::isCrumbValid();
        $malformedCrumb = 'wxyz';
        $malformedCrumbResult = self::isCrumbValid( $malformedCrumb);
        
        return array(
            'CRUMB_PATTERN' => self::CRUMB_PATTERN,       
            'goodCrumb' =>      array('crumb' => $goodCrumb,      'isValid' => $goodCrumbResult ),
            'badCrumb' =>       array('crumb' => $badCrumb,       'isValid' => $badCrumbResult),
            'nullCrumb' =>      array('crumb' => null,            'isValid' => $nullCrumbResult),
            'malformedCrumb' => array('crumb' => $malformedCrumb, 'isValid' => $malformedCrumbResult)
        );
    }
    
} // end class Crumb


