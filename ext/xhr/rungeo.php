<?php

include_once('bootstrap.inc.php');
include_once('inc/cache/DRCache.inc.php');
include_once('geo/GeoLookup.inc.php');

$TEST_ZIP_CODE = '90230';
$MAKE_FAIL_ZIP_CODE = '9x040';
$MAKE_ZER0_SET_ZIP_CODE = '00000';
$MALFORMED_ZIP_CODE = 'FourScoreAndSevenYearsAgo';

$zipcodeTestArr = array(
    '94401',
    '94402',
    '94403',
    '94404',
    '94080',
    '94062',
    '95008');

for($i=0; $i<= 2; $i++)
{
    $j = 0;
    foreach( $zipcodeTestArr as $zipcode )
    {
        print "[$i,$j] ";
        $err = array();
        $result = DRGeoLookup::getLocationByZipcode($zipcode,$err);
        if( false == $result )
        {
            print "Error: " . var_export($err,true)."\n";
        }
        else
        {
            print "Restult: " .var_export($result,true). "\n";
        }
        $j++;
    }
}


/*
$location = DRGeoLookup::getLocationByZipcode($TEST_ZIP_CODE,$err);
//$location = DRGeoLookup::getLocationByZipcode($MAKE_FAIL_ZIP_CODE,$err);
//$location = DRGeoLookup::getLocationByZipcode($MAKE_ZER0_SET_ZIP_CODE,$err);
//$location = DRGeoLookup::getLocationByZipcode($MALFORMED_ZIP_CODE,$err);
//$location = DRGeoLookup::getLocationByZipcode(null,$err);

print "Run Geo test\n";

if( false != $location )
{
    print "Run Geo sees location=" .var_export($location,true) ."\n";
}
else
{
    print "Run Geo sees location ERROR=" .var_export($err,true) ."\n";
}
*/
print "End\n";