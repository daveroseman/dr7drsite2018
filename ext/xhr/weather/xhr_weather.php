<?php

class WebWeatherException extends Exception{}
class EmptyZipCodeListException extends WebWeatherException{}

interface WebWeatherIntf
{
    static function getWeather($loc,&$errPtr);
}

abstract class WebWeather implements WebWeatherIntf
{
    const ERROR = 'error';
    const FAULT = 'fault';
}

abstract class NOAAWeather extends WebWeather implements WebWeatherIntf
{
    const NOAA_REST_ENDPOINT = 'http://graphical.weather.gov/xml/sample_products/browser_interface/ndfdBrowserClientByDay.php';
    const FETCH_NUM_DAYS = 5;
   
    protected static function _fetchWeather($zipCodeList,&$err)
    {
        // Validate zip code list here
        $url = self::getWeatherUrl($zipCodeList,$err);
        $response = file($url);
        return $response;
    }
    
    protected static function getWeatherURL($zipCodeList=array())
    {
        if(count($zipCodeList) == 0)
        {
            throw new EmptyZipCodeListException();
            return;
        }
        
        $zipCodeString = implode('+',$zipCodeList);
        
        return self::NOAA_REST_ENDPOINT . 
            '?zipCodeList=' . $zipCodeString . 
            '&format=12+hourly&numDays=' . self::FETCH_NUM_DAYS .
            '&Unit=e';
    }
    
    protected static function isLongRangeTimeLayoutKey($key)
    {
        $parts = split('-',$key);
        array_pop($parts);
        $numericPart = array_pop($parts);
        return ('n10' === $numericPart);
    }
} // end class NOAAWeather

class SummarizedNOAAWeather extends NOAAWeather implements WebWeatherIntf
{

    static function getWeather($zipCodeList,&$err) // throws WebWeatherException
    {
        return self::fetchWeather($zipCodeList,&$err);
        
        // To do:  Cache data here.
    }
    
    static function fetchWeather($zipCodeList,&$err)
    {
        $responseArr = parent::_fetchWeather($zipCodeList,$err); // Requires timeout on I/O.
        $xml = implode("",$responseArr);

       // TODO:  Detect non-xml here.
        
        return self::getSummarizedNOAAWeatherXML($zipCodeList,$xml,$err);
    }
    
    static function getSummarizedNOAAWeatherXML($zipcodeRequestList,$xmlStr='',&$err)
    {
        $weather = self::_getFullSummarizedNOAAWeatherXML($zipcodeRequestList,$xmlStr,$err);
 
        // Massage data here.
        //////////////////////////////////////
        if( isset( $weather[LOCATION_REQUEST_LIST][REQUESTED_LOCATION] ) )
        {
            foreach($weather[LOCATION_REQUEST_LIST][REQUESTED_LOCATION] as $loc) // Assume loc is a zipcode for now.
            {
            
            }
        }
        
        return $weather;
    }
    
    private static function _getFullSummarizedNOAAWeatherXML($zipcodeRequestList,$xmlStr='',&$err)
    {
        $fetchTime = time();
        if(empty($xmlStr)){ return false; }

        $weather = array();
        $iter = new SimpleXMLIterator($xmlStr);
        
        /* START DECODER */ 
        
        if( isset($iter->error))
        {
            $fault = (string)$iter->error;
            $err = array(
                'error' => 'fault',
                'fault' => $fault,
                FETCH_TIME => $fetchTime
            ); 
            return false;
        }
        
        if( !isset($iter->data))
        {
            $err = array(
                'error' => 'no_data',
                FETCH_TIME => $fetchTime
            );
            return false;
        }
        
        if( !isset($iter->data->location))
        {
            $err = array(
                'error' => 'no_location_data',
                FETCH_TIME => $fetchTime
            );
            return false;
        }
        
        $weather[METADATA] = array();
        
        // Enter the fetch time.
        $weather[FETCH_TIME] = $fetchTime;

        /* Find geolocations:
        */
        $weather[LOCATION_REQUEST_LIST] = $zipcodeRequestList;
        
        foreach($iter->data->location as $node)
        {
            $key = (string)$node->{'location-key'};
            $lat = (string)$node->point['latitude'];
            $lon = (string)$node->point['longitude'];
            $reqZipcode = array_shift($zipcodeRequestList);
            $zipcodeLookupErr = array();
            $zipcodeLookupResult = DRGeoLookup::getLocationByZipcode($reqZipcode,$zipcodeLookupErr);
            $zipcodeLookupErr = ( count($zipcodeLookupErr) > 0 ? $zipcodeLookupErr : false );
            
            $weather[LOCATION_LIST][$key] = array(
                GEOLOCATION => array(
                    'lat' => $lat,
                    'lon' => $lon 
                ),
                REQUESTED_LOCATION => array(
                    ZIPCODE => $reqZipcode,
                    ZIPCODE_LOOKUP_RESULT => $zipcodeLookupResult,
                    ZIPCODE_LOOKUP_ERROR  => $zipcodeLookupErr
                )
            );
        } // end foreach over location
        
        foreach($iter->data->moreWeatherInformation as $mwInfoNode)
        {
            $thisLocKey = (string)$mwInfoNode['applicable-location'];
            $extUrl = (string)$mwInfoNode;
            $weather[LOCATION_LIST][$thisLocKey]['moreWeatherInformation'] = $extUrl;
        }
    
        
        if(isset($iter->data->{'time-layout'}))
        {
            $weather[TIMESPAN] = array();
           
            foreach($iter->data->{'time-layout'} as $timeNode)
            {
                $timeNodeIndex = 0;
                foreach($timeNode as $kData => $vData)
                {
                    $key =(string)$kData;
                    switch($key)
                    {
                        case 'layout-key':
                            $layoutKey = (string)$timeNode->{'layout-key'};
                            
                            if(!isset($weather[TIMESPAN][$layoutKey]))
                            {
                                $weather[TIMESPAN][$layoutKey] = array();
                            }
                            
                            break;
                            
                        case 'start-valid-time':
                            $startTime =  (string)$timeNode->{'start-valid-time'};
                            $weather[TIMESPAN][$layoutKey][$timeNodeIndex]['start-valid-time'] = $startTime;
                            
                            if( isset( $timeNode->{'start-valid-time'}['period-name']) )
                            {
                                $periodName = (string)$timeNode->{'start-valid-time'}['period-name'];
                                $weather[TIMESPAN][$layoutKey][$timeNodeIndex]['period-name'] = $periodName;
                            }
                            else
                            {
                                $periodName='undef';
                            }

                            break;
                            
                        case 'end-valid-time':
                            $endTime =  (string)$timeNode->{'end-valid-time'};
                            $weather[TIMESPAN][$layoutKey][$timeNodeIndex]['end-valid-time'] = $endTime;
                            // Advance the index:
                            $timeNodeIndex++;
                            break;
                            
                        default:
                            print "timeNode key=$key not handled.\n";
                            
                    }// end switch block
                }
            }
        }//end iter over time layout
        
        $weather[METADATA]['_param_count'] = count($iter->data->parameters);
        // Iterate over weather parameters
        $weather[WEATHER_PARAMS] = array();
        $idx = 0;
        foreach($iter->data->parameters as $pNode)
        {               
            $nodeName = (string)$pNode->getName();
            $forLoc = (string)$pNode['applicable-location'];
            if( !isset( $weather[WEATHER_PARAMS][$forLoc] ) )
            {
                $weather[WEATHER_PARAMS][$forLoc] = array('bee' => 'bop');
            }
          
            foreach($pNode as $mNode)
            {
                $mNodeName = (string)$mNode->getName();
                $timeLayout = (string)$mNode['time-layout'];
                
                if(!isset($weather[WEATHER_PARAMS][$forLoc][$timeLayout]))
                {
                    $weather[WEATHER_PARAMS][$forLoc][$timeLayout] = array('foo' => 'bar');
                }
                else{
                    //print "DEBUG condition not met.\n";
                }
               
                switch($mNodeName)
                {
                    case 'temperature':
                        $type =       (string)$mNode['type'];
                        $units =      (string)$mNode['units'];
                        if(!isset($weather[METADATA]['_temperature_units']))
                        {
                            $weather[METADATA]['_temperature_units'] = $units;
                        }
                        
                        if(!isset($weather[WEATHER_PARAMS][$forLoc][$timeLayout][TEMPERATURE]))
                        {
                            $weather[WEATHER_PARAMS][$forLoc][$timeLayout][TEMPERATURE] = array();
                        }
                        
                        if(!isset($weather[WEATHER_PARAMS][$forLoc][$timeLayout][TEMPERATURE][$type]))
                        {
                            $weather[WEATHER_PARAMS][$forLoc][$timeLayout][TEMPERATURE][$type] = array();
                        }
                        foreach($mNode->value as $value)
                        {
                            $weather[WEATHER_PARAMS][$forLoc][$timeLayout][TEMPERATURE][$type][] = (string)$value;
                        }
                        print "\n";
                        break;
                        
                    case 'probability-of-precipitation':
                        $type =       (string)$mNode['type'];
                        $units =      (string)$mNode['units'];
                        if(!isset($weather[METADATA]['_precip_units']))
                        {
                            $weather[METADATA]['_precip_units'] = $units;
                        }
                      
                        if(!isset($weather[WEATHER_PARAMS][$forLoc][$timeLayout][PRECIPITATION]))
                        {
                            $weather[WEATHER_PARAMS][$forLoc][$timeLayout][PRECIPITATION]= array();
                        }
                        foreach($mNode->value as $value)
                        {
                            $weather[WEATHER_PARAMS][$forLoc][$timeLayout][PRECIPITATION][] = (string)$value;
                        }
                        print "\n";
                        break;
                        
                    case 'hazards': // Ignore for now.
                        break;
                        
                    case 'weather':
                           $wNodeName =  (string)$mNode->getName();

                           if(!isset($weather[WEATHER_PARAMS][$forLoc][$timeLayout][WEATHER]))
                           {
                                $weather[WEATHER_PARAMS][$forLoc][$timeLayout][WEATHER] = array();
                           }
                           
                           foreach($mNode->{'weather-conditions'} as $cNode)
                           {
                                $weatherSummary = (string)$cNode['weather-summary'];
                                $weather[WEATHER_PARAMS][$forLoc][$timeLayout][WEATHER][] = $weatherSummary;
                           }
                           
                            break;
                        
                    case 'conditions-icon':
                            if(!isset($weather[WEATHER_PARAMS][$forLoc][$timeLayout][WEATHER_CONDITIONS_ICON_URL]))
                            {
                                $weather[WEATHER_PARAMS][$forLoc][$timeLayout][WEATHER_CONDITIONS_ICON_URL] = array();
                            }
                            foreach($mNode->{'icon-link'} as $iconUrl)
                            {
                                $weather[WEATHER_PARAMS][$forLoc][$timeLayout][WEATHER_CONDITIONS_ICON_URL][] = (string)$iconUrl;
                            }
                        break;
                        
                    default:
                        $err[] = "Param key '".$paramKey."' unhandled.";
                }// end switch block
                
            } // end foreach($pNode as $mNode)

          
             $idx++;
        } // end foreach(params as paramKey => paramNode)
        
        return $weather;
        
    } // end function getSummarizedNOAAWeatherXML()
} // end class

/*
class BreifSummarizedNOAAWeather extends SummarizedNOAAWeather implements WebWeatherIntf
{
    static function getWeather($zipCodeList,&$err) // throws WebWeatherException
    {
        return self::fetchWeather(array('02134','80809'),&$err);//(array('90230','90210'),$err);
    }
    
}// end class BreifSummarizedNOAAWeather
*/