<?php

class HttpSetHeaders
{
    static function setJson()
    {
        /* Set HTTP header to json, no caching permitted. */
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
    }
}   