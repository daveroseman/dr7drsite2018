<?php

class TestException extends Exception{}

class JsonPrinter
{
    var $newParams;
    
    function __construct(){}
    
    function addParams($newParams)
    {
        if(!is_array($newParams))
        {
            throw new TestException("New params must be an array");
            return;
        }
        $this->newParams = $newParams; 
    }
    
    private function _getParams()
    {
        $jsonTestParams =  array(
            'is_json_test' => true,
            'time' => time(),
            'is_cached' => false);
            
        if( is_array( $this->newParams ))
        {
            $jsonTestParams = array_merge($jsonTestParams,$this->newParams);
        }

        return $jsonTestParams;
    }
    
    function printReponse() // Exits by default
    {
        HttpSetHeaders::setJson();
        $arr = $this->_getParams();
        //print var_export($arr,true)."\n";
        print json_encode($arr);
        exit(0);
    }
}
