/* drsite_hd.js 
   2018-09-06 */

var DRSITE = DRSITE || {};
DRSITE.weather = {};  

(function($) {   
   $(document).ready( function() {
        $('#subfooter-container span.control').click(function(e){
            e.preventDefault();
            $('div.region-subfooter').toggle('fast',function(){
                
                $('a.minus').toggle();
                $('a.plus').toggle();
                
            });
        });   
   }); // ready  
}) ( jQuery ); 

(function($) {  
   // we can now rely on $ within the safety of our "bodyguard" function  
   $(document).ready( function() {  
        //----------------------
        DRSITE.notopt = {
            init: function(){
                $('#not-opt div').click(function(){
                    $(this).hide();
                });
            }
        };
        
        //----- Weather app --------------
        // Weather app temporarily moved to drsite_weather.js for development.

        // Initialize objects:
        DRSITE.notopt.init();
        //---------------------------
   }); // ready  
}) ( jQuery ); 

