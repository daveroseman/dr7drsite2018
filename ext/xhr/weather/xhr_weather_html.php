<?php

class WebWeatherHtmlException extends Exception{}

interface WebWeatherHtmlIntf
{
   static function getWeatherHtml($loc,&$errPtr);
}

class WebWeatherHtml implements WebWeatherHtmlIntf
{
   static function getWeatherHtml($loc,&$errPtr)
   {
        return self::fetchWeatherHtml($loc,$errPtr);
        
        // cache here
   }
   
   static function fetchWeatherHtml($loc,&$errPtr)
   {
        $html = '';
        $weather = SummarizedNOAAWeather::getWeather($loc,$errPtr);
        //print_r($weather);
        $html .= var_export($weather,true);
        $html .= '<div class="wrap weatherwrap">'."\n";
        $html .= '  <div class="current">'."\n";        
        $html .= '    <h4>Current weather</h4>.'."\n";
        $html .= '    <span class="city">Anytown</span><span class="low">10</span><span class="current_temp">42&deg;</span><span class="high">72</span>' . "\n";
        $html .= '  </div><!-- /current -->' ."\n";
        $html .= '  <div class="forecast">'."\n";  
        $html .= '     <h3>Forecast.</h3>'."\n";
        $html .= '     <table>'."\n";
        $html .= '       <tr>'."\n";
        $html .= '         <th>table head</th>'."\n";
        $html .= '       </tr>'."\n";
        $html .= '       <tr>'."\n";
        $html .= '         <td>table display</td>'."\n";
        $html .= '       </tr>'."\n";
        $html .= '     </table>'."\n";
        $html .= '  </div><!-- /forecast -->' ."\n";
        $html .= '</div><!-- /weatherwrap -->' . "\n";
        return $html;
   }
   
} // end class