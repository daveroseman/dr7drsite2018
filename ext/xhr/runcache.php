<?php
  
include_once('bootstrap.inc.php');
include_once('inc/cache/DRCache.inc.php');
print "Run cache:\n";

$cache = DRSqlCache::getInstance();
$type = $cache->getType();
print "Cache type=$type \n";

$get = $cache->getNow();
print "Get=" . var_export($get,true)."\n";

print "Clean cache NOW:\n";
$cache->clean();

$regularPutArr = array(

    array(
        'topic' => 'zipcode',
        'key' =>'90230', 
        'value' => 'Culver City'
        // omit expiry
    ),
    array(
        'topic' => 'zipcode',
        'key' =>'90232', 
        'value' => 'Culver City',
        EXPIRY => ZIPCODE_GEODATA_CACHE_EXPIRES
    ),
    array(
        'topic' => 'weather',
        'key' =>'90230', 
        'value' => 'Partly Cloudy',
        EXPIRY => WEATHER_DATA_CACHE_EXPIRES
    ),
    array(
        'topic' => 'weather',
        'key' =>'94043', 
        'value' => 'Sunny',
        EXPIRY => -1
    ),
    array(
        'topic' => 'weather',
        'key' =>'90231', 
        'value' => array('precip' => 'rain','temp' => '42dF') ,
        EXPIRY => ZIPCODE_GEODATA_CACHE_EXPIRES)
);
print "Start regular put arr: \n";

foreach( $regularPutArr as $data )
{
    $topic = $data['topic'];
    $key   = $data['key'];
    $value = $data['value'];
    $expiry = (isset($data[EXPIRY]) ? $data[EXPIRY] : 0);
    
    if($expiry>0)
    {
        $cache->put($topic,$key,$value,$expiry);
    }
    else
    {
        $cache->put($topic,$key,$value);
    }
}

$regularQueryArr = array();
$regularQueryArr[] = array('topic' => 'weather', 'ckey' => '90230');
$regularQueryArr[] = array('topic' => 'weather', 'ckey' => '90231');
$regularQueryArr[] = array('topic' => 'weather', 'ckey' => '90232');
$regularQueryArr[] = array('topic' => 'weather', 'ckey' => '90403');
$regularQueryArr[] = array('topic' => 'zipcode', 'ckey' => '90230');
$regularQueryArr[] = array('topic' => 'zipcode', 'ckey' => '90231');
$regularQueryArr[] = array('topic' => 'zipcode', 'ckey' => '90232');
$regularQueryArr[] = array('topic' => 'zipcode', 'ckey' => '90403');

print "Start regular queries:\n";

$count=0;
foreach($regularQueryArr as $data )
{
    $topic = $data['topic'];
    $cKey =  $data['ckey'];
    $get = $cache->get($topic,$cKey);
    print $count++ . ". Ask: topic=$topic, key=$cKey\nGet=" . var_export($get,true)."\n\n";
}



print "End\n";